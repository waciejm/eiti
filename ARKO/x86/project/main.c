// Maciej Wojno 302692
// Duzy projekt INTEL
// Szyfrowanie i deszyfrowanie szachownicy Polibiusza

#include <stdio.h>

#include "x86_64_polibiusz.h"

int text_in_alphabeth(char* alphabeth, char* text) {
	for (int t = 0; text[t] != 0; ++t) {
		int in_alphabeth = 0;
		for (int a = 0; alphabeth[a] != 0; ++a) {
			if (text[t] == alphabeth[a]) {
				in_alphabeth = 1;
				break;
			}
		}
		if (!in_alphabeth) {
			return 0;
		}
	}
	return 1;
}

int cipher_in_table(char* alphabeth, char* key, char* cipher) {
	int a_len;
	for (a_len = 0; alphabeth[a_len] != 0; ++a_len) {};
	int key_len;
	for (key_len = 0; alphabeth[key_len] != 0; ++key_len) {};

	int table_length = 10 + a_len - key_len;

	for (int i = 0; cipher[i] != 0; ++i) {
		while (cipher[i] == ' ') ++i;
		char first = cipher[i];
		if (first == 0) break;
		if (first < '0' || first > '9') return 0;
		int f_index = ((first - '0' + 9) % 10);
		if (f_index >= key_len) {
			++i;
			while (cipher[i] == ' ') ++i;
			int second = cipher[i];
			if (second < '0' || second > '9') return 0;
			int s_index = ((first - '0' + 9) % 10);
			int pos = (f_index - key_len) * 10 + s_index;
			if (pos >= table_length) return 0;
		}
	}

	return 1;
}

int main() {

	char* alphabeth = "abcdefghijklmnopqrstuvwxyz ";

	char action[3] = {};
	do {
		printf("Wybierz akcje (s - szyfrowanie / d - deszyfrowanie): ");
	} while (scanf("%s", action) != 1 || (action[0] != 's' && action[0] != 'd'));

	char key[10] = {};
	do {
		printf("Podaj klucz: ");
	} while (scanf("%s", key) > 8 || !text_in_alphabeth(alphabeth, key));

	char message[100] = {};
	if (action[0] == 's') {
		do {
			scanf("%c", message);
			printf("Podaj wiadomosc do zaszyfrowania: ");
			scanf("%[^\n]", message);
		} while (!text_in_alphabeth(alphabeth, message));
	} else {
		do {
			scanf("%c", message);
			printf("Podaj szyfrogram do odszyfrowania: ");
			scanf("%[^\n]", message);
		} while (!cipher_in_table(alphabeth, key, message));
	}

	char answer[300] = {};
	x86_64_polibiusz(action[0], alphabeth, key, message, answer);

	if (action[0] == 's') {
		printf("Szyfrogram: %s\n", answer);
	} else {
		printf("Wiadomosc: %s\n", answer);
	}

	return 0;
}
