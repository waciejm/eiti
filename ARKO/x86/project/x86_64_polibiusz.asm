; Maciej Wojno 302692
; Duzy projekt INTEL
; Szyfrowanie i deszyfrowanie szachownicy Polibiusza

	section .text
	global x86_64_polibiusz

x86_64_polibiusz:
	push rbp
	mov rbp, rsp
	sub rsp, 88

	mov [rbp - 8], rdi	; action
	mov [rbp - 16], rsi	; alphabeth_pointer
	mov [rbp - 24], rdx	; key_pointer
	mov [rbp - 32], rcx	; message_pointer
	mov [rbp - 40], r8	; answer_pointer
	;    rbp - 80		; coding_table
	;    rbp - 88		; key_len

	push rbx
	push r12
	push r13
	push r14
	push r15

;------ zapelnienie tablicy kodowej zerami
	mov r8, rbp	; table
	sub r8, 80	;
	mov r9, 0	; table index
clear_table_loop:
	mov qword [r8 + r9], 0
	add r9, 8
	cmp r9, 40
	jl clear_table_loop
;------

;------ budowa tablicy kodowej
;------------ wpisanie klucza
	mov r8, [rbp - 24] 	; key
	mov r9, 0			; key index
	mov r10, rbp		; table
	sub r10, 80			;
	mov r11, 0			; table index

	cmp byte [r8], 0
	je table_add_key_end
table_add_key_loop:
	mov r12b, byte [r8 + r9]	; przepisanie znaku
	mov byte [r10 + r11], r12b	; z klucza do tablicy
	inc r9
	inc r11
	cmp byte [r8 + r9], 0
	jne table_add_key_loop
table_add_key_end:
;------------
;------------ wpisanie reszty
	mov r8, [rbp - 16]		; alphabeth pointer
	mov r9, 0				; alphabeth index
	mov r10, [rbp - 24]		; key pointer
	mov r11, 0				; key index
	mov r12, rbp			; table
	sub r12, 80				;
	mov r13, 10				; table index
table_next_sign:
	mov r11, 0
table_next_key_sign:
	cmp byte [r10 + r11], 0		; sprawdzenie czy przeszukano caly klucz
	je table_end_of_key
	mov r14b, byte [r8 + r9]	; sprawdzenie czy znak pojawia sie w kluczu
	cmp byte [r10 + r11], r14b	;
	je table_next_sign_with_increment
	inc r11						; przejscie do porownania z nastepnym znakiem klucza
	jmp table_next_key_sign
table_end_of_key:
	mov r14b, byte [r8 + r9]		; zapisanie kolejnego znaku w tablicy
	mov byte [r12 + r13], r14b	;
	inc r13
table_next_sign_with_increment:
	inc r9						; przejscie do nastepnego znaku alfabetu
	cmp byte [r8 + r9], 0		; sprawdzenie czy skonczyl sie alfabet
	jne table_next_sign
;------------
;------

;------ liczenie dlugosci klucza
	mov r8, [rbp - 24]	; key
	mov r9, 0			; key index
	cmp byte [r8 + r9], 0
	je key_len_end
key_len_loop:
	inc r9
	cmp byte [r8 + r9], 0
	jne key_len_loop
key_len_end:
	mov qword [rbp - 88], r9
;------

	cmp byte [rbp - 8], 'd'	; wybor akcji
	je decypher				;

;------ szyfrowanie
cypher:
	mov r8, [rbp - 32]	; message
	mov r9, rbp		; table
	sub r9, 80		;
	mov r10, 0			; table index
	mov r11, [rbp - 40]	; answer
	mov r12, [rbp - 88]	; key_len
	cmp byte [r8], 0 ; sprawdzenie konca wiadomosci
	je cypher_end
cypher_loop:
	mov r10, -1
	cmp byte [r8], 0	; sprawdzenie konca wiadomosci
	je cypher_end		;
	mov r13b, byte [r8]	; wczytanie kolejnego znaku z wiadomosci
cypher_loop_inner:
	inc r10
	cmp r10, 40		; nie znaleziono znaku w szachownicy
	je finish		; awaryjne zakonczenie
	cmp r13b, byte [r9 + r10]	; sprawdzenie czy znalezlismy znak w szachownicy
	jne cypher_loop_inner		;
	cmp r10, r12	
	jge c_outside_key
c_inside_key:
	mov r13b, r10b			;
	add r13b, '1'			; zapisanie znaku do szyfrogramu
	mov byte [r11], r13b	;
	mov byte [r11 + 1], ' '	; zapisanie spacji
	add r11, 2		; przejscie do nastepnego znaku w szyfrogramie
	inc r8		; przejscie do nastepnego znaku we wiadomosci
	cmp byte [r8], 0	; sprawdzenie konca wiadomosci
	je cypher_end		;
	jmp cypher_loop
c_outside_key:
	mov r15w, 10
	mov ax, r10w
	mov dx, 0
	div r15w
	mov r13w, ax	; wiersz
	add r13w, r12w	;
	mov r14w, dx	; kolumna
	add r14w, 1		;
	mov ax, r14w	;
	mov dx, 0		;
	div r15w			;
	mov r14w, dx	;
	add r13w, '0'			; zapisanie wiersza
	mov byte [r11], r13b	;
	add r14w, '0'				; zapisanie kolumnt
	mov byte [r11 + 1], r14b	;
	mov byte [r11 + 2], ' '		; zapisanie spacji
	add r11, 3	; przejscie do nastepnych znakow w szyfrogramie
	inc r8		; przejscie do nastepnego znaku we wiadomosci
	cmp byte [r8], 0	; sprawdzenie konca wiadomosci
	jne cypher_loop		;
cypher_end:
	mov byte [r11], 0
	jmp finish
;------

;------ deszyfrowanie
decypher:
	mov r8, [rbp - 32]	; message
	mov r9, rbp		; table
	sub r9, 80		;
	mov r10, [rbp - 40]	; answer
	mov r11, [rbp - 88]	; key_len
	cmp byte [r8], ' '	; usuniecie spacji na poczatku
	je d_eliminate_space
	cmp byte [r8], 0 ; sprawdzenie konca szyfrogramu
	je decypher_end
decypher_loop:
	mov r12, 0			;
	mov r12b, byte [r8]	; wczytanie znaku wiadomosci
	sub r12, '0'		;
	add r12, 9			; zamiana na poprawny indeks
	mov ax, r12w		;
	mov dx, 0			;
	mov r13w, 10		;
	div r13w			;
	mov r12w, dx		;
	cmp r12, r11		; sprawdzenie czy znak w kluczu
	jge d_outside_key
d_in_key:
	mov r13b, byte [r9 + r12]	; wpisanie zdekodowanego znaku do wiadomosci
	mov byte [r10], r13b		;
	inc r10		; przesuniecie miejsca w odpowiedzi
	inc r8		; szesuniecie miejsca w szyfrogramie
	cmp byte [r8], ' '		; sprawdzenie czy trzena usunac spacje
	je d_eliminate_space	;
	cmp byte [r8], 0	; sprawdzenie konca wiadomosci
	je decypher_end		;
	jmp decypher_loop	; przejscie do deszyfrowania nastepnego znaku
d_outside_key:
	sub r12, r11	; zamiana indeksu wiersza na poprawny
	inc r12			;
	mov r13, 0				;
	mov r13b, byte [r8 + 1]	; wczytanie drugiego znaku
	sub r13, '0'		;
	add r13, 9			; zamiana na poprawny indeks kolumny
	mov rax, 0			;
	mov ax, r13w		;
	mov dx, 0			;
	mov r14w, 10		;
	div r14w			;
	mov r13w, dx		;
	mov ax, 10		; wylicznie indeksu w tablicy
	mul r12w		;
	add ax, r13w	;
	mov r15b, [r9 + rax]	; odczytanie znaku z szachownicy
	mov byte [r10], r15b	; zapisanie znaku w odpowiedzi
	inc r10		; przesuniecia miejsca w odpowiedzi
	add r8, 2	; przesuniecie miejsca w szyfrogramie
	cmp byte [r8], 0	; sprawdzenie konca szyfrogramu
	je decypher_end		;
	cmp byte [r8], ' '	; sprawdzenie czy trzeba usunac spacje
	jne decypher_loop	;
d_eliminate_space:
	inc r8
	cmp byte [r8], ' '
	je d_eliminate_space
	cmp byte [r8], 0
	jne decypher_loop
decypher_end:
;------

finish:
	pop r15
	pop r14
	pop r13
	pop r12
	pop rbx
	leave
	ret

