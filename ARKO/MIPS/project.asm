# Maciej Wojno 302692
# Duzy projekt MIPS
# Szyfrowanie i deszyfrowanie szachownicy Polibiusza

		.data
t_act:		.asciiz "Wybierz akcje (s - szyfrowanie / d - deszyfrowanie): "
t_key:		.asciiz "Podaj klucz: "
a_cypher_ask:	.asciiz "Podaj wiadomosc do zaszyfrowania: "
a_decypher_ask:	.asciiz "Podaj szyfrogram do odszyfrowania: "
a_cypher_ans:	.asciiz "Szyfrogram: "
a_decypher_ans:	.asciiz "Odszyfrowana wiadomosc: "
m_err_in_msg:	.asciiz "Znaleziono znak spoza alfabetu!"
m_err_in_cry:	.asciiz "Bledny szyfrogram!"
m_err_key_len:	.asciiz "Klucz za dlugi!"

alphabeth:	.asciiz "abcdefghijklmnopqrstuvwxyz "
	
		.text
		.globl main
	
main:
	la $fp, 0($sp)
	
#-# WYBRANIE AKCJI
ask_for_action:
	la $a0, t_act
	li $v0, 4			# wypisanie zapytania o akcje
	syscall
	
	li $v0, 12			# wczytanie akcji
	syscall
	
	la $t0, 0($v0)			# zapisanie akcji do rejestru t0
	
	li $a0, '\n'
	li $v0, 11			# wypisanie '\n'
	syscall
	
	beq $t0, 's', correct_action	# sprawdzenie poprawnosci wybranej akcji
	beq $t0, 'd', correct_action
	b ask_for_action
	
correct_action:
	addi $sp, $sp, -4		# ACTION: -4
	sb $t0, ($sp)			# zapisanie akcji na stosie
#-#

#-# ALOKACJA SZACHOWNICY POLIBIUSZA I WPISANIE KLUCZA

#-----# ALOKACJA I ZEROWANIE TABLICY
	addi $sp, $sp, -40		# TABLE: -44
	
	li $t0, 0			# adres wzgledem poczatku tablicy
clear_table_loop:
	add $t1, $sp, $t0		# adres w tablicy
	sw $zero, 0($t1)		# zapisanie zerem
	addi $t0, $t0, 4		# przesuniecie wskaznika
	blt $t0, 40, clear_table_loop
#-----#

#-----# WCZYTYWANIE KLUCZA
	la $a0, t_key
	li $v0, 4			# wypisanie zapytania o klucz
	syscall
	
	la $a0, 0($sp)
	li $a1, 40			# wczytanie klucza
	li $v0, 8
	syscall
#-----#

#-----# LICZENIE DUGOSCI KLUCZA
	li $t0, -1			# dlugosc klucza
key_length_loop:
	addi $t0, $t0, 1
	add $t1, $sp, $t0
	lb $t2, 0($t1)
	bne $t2, '\n', key_length_loop
	sb $zero, 0($t1)		# nadpisanie '\n' na koncu stringa zerem

	bgt $t0, 8, error_key_too_long	# sprawdzenie dlugosci klucza
	addi $sp, $sp, -4		# KEY_LEN: -48
	sw $t0, -48($fp)		# zapisanie dlugosci klucza na stos
#-----#
	
#	la $a0, -44($fp)		#
#	li $v0, 4			# DEBUG wypisanie klucza
#	syscall				#
#	
#	li $a0, '\n'			#
#	li $v0, 11			# DEBUG wypisanie '\n'
#	syscall				#
#	
#	lw  $a0, -48($fp)		#
#	li $v0, 1			# DEBUG wypisanie dlugosci klucza
#	syscall				#
#	
#	li $a0, '\n'			#
#	li $v0, 11			# DEBUG wypisanie '\n'
#	syscall				#
	
#-#

#-# WYPELNIANIE RESZTY TABLICY ZNAKAMI
	la $t0, alphabeth 		# zaladowanie pierwszego znaku w alfabecie
	lb $t4, 0($t0)			#
	la $t1, -34($fp)		# miejsce na kolejny znak w tablicy -44 + 10
	
#-----# SPRAWDZENIE CZY ZNAK JEST W KLUCZU I ZAPISANIE JESLI NIE MA
next_sign:
	la $t2, -44($fp)		# znak w kluczu
next_key_sign:
	lb $t3, 0($t2)				# sprawdzenie czy koniec klucza
	beqz $t3, end_of_key			# jesli tak - przejscie do nastepnego znaku
	beq $t4, $t3, next_sign_with_increment	# przejscie do nastepnego znaku jesli znaleziono taki sam znak w kluczu
	addi $t2, $t2, 1			# przejscie do kolejnego znaku w kluczu
	b next_key_sign				# przejscie do sprawdzenia nastepnego znaku w kluczu
end_of_key:
	sb $t4, 0($t1)			# zapisanie kolejnego znaku w tablicy
	addi $t1, $t1, 1		# przesuniecie miejsca na znak w tablicy
next_sign_with_increment:
	addi $t0, $t0, 1		# przejscie do nastepnego znaku alfabetu
	lb $t4, 0($t0)			# wczytanie kolejnego znaku
	bnez $t4, next_sign		# sprawdzenie konca znakow
#-----#

fill_end:

#	la $a0, -34($fp)		#
#	li $v0, 4			# DEBUG wypisanie reszty znakow w tablicy
#	syscall				#
#	
#	li $a0, '\n'			#
#	li $v0, 11			# DEBUG wypisanie '\n'
#	syscall				#

#-#

#-# WYKONANIE WYBRANEJ AKCJI
	lb $t0, -4($fp)
	beq $t0, 'd', decypher

#-----# SZYFROWANIE
cypher:
	la $a0, a_cypher_ask		#
	li $v0, 4			# Wypisanie zaptytania o wiadomosc
	syscall				#
	
	addi $sp, $sp, -100		# MESSAGE: -148
	
	la $a0, -148($fp)		#
	li $a1, 100			# Wczytanie wiadomosci 
	li $v0, 8			#
	syscall				#
	
	addi $sp, $sp, -300		# CRYPTOGRAM: -448
	
#---------# GENEROWANIE SZYFROGRAMU
	la $t0, -148($fp)		# miejsce znaku we wiadomosci
	la $t1, -448($fp)		# miejsce znaku w kryptografie
	lw $t7, -48($fp)		# dlugosc klucza

	lb $t4, 0($t0)			# wczytanie znaku z wiadomosci
	beq $t4, '\n', finish_cypher	# sprawdznie konca wiadomosci
cypher_loop:
	li $t2, -1			# indeks w szachownicy
cypher_loop_inner:
	addi $t2, $t2, 1		# zwiekszenie indeksu w szachownicy
	
	beq $t2, 40, error_in_message	# nie znaleziono znaku w szachownicy
	
	add $t3, $t2, $fp		# wczytanie znaku z szachownicy
	lb $t3, -44($t3)		#
	
	bne $t3, $t4, cypher_loop_inner	# przejscie do kolejnego znaku w szachownicy jesli nie znaleziono znaku
	
#-------------# ZAKODOWANIE ZNAKU
	addi $t0, $t0, 1		# przejscie do nastepnego znaku we wiadomosci
	blt $t2, 10, c_in_key
c_outside_key:
	div $t5, $t2, 10		# wiersz w szachownicy
	add $t5, $t5, $t7		#
	rem $t6, $t2, 10		# kolumna w szachwnicy
	addi $t6, $t6, 1		#
	rem $t6, $t6, 10		#
	
	rem $t8, $t5, 10		# zamiana 10 na 0
	addi $t8, $t8, '0'		# zapisanie wiersza
	sb $t8, 0($t1)			#
	addi $t8, $t6, '0'		# zapisanie kolumny
	sb $t8, 1($t1)			#
	li $t8, ' '			# zapisanie spacji
	sb $t8, 2($t1)			#
	addi $t1, $t1, 3		# zwiekszenie wskaznika znaku w szyfrogramie
			
	lb $t4, 0($t0)			# wczytanie znaku z wiadomosci
	bne $t4, '\n', cypher_loop	# sprawdznie konca wiadomosci
	b finish_cypher
c_in_key:
	addi $t5, $t2, '1'		# zapisanie znaku
	sb $t5, 0($t1)			#
	li $t5, ' '			# zapisanie spacji
	sb $t5, 1($t1)			#
	addi $t1, $t1, 2		# zwiekszenie wskaznika znaku w szyfrogramie
		
	lb $t4, 0($t0)			# wczytanie znaku z wiadomosci
	bne $t4, '\n', cypher_loop	# sprawdznie konca wiadomosci
#-------------#

finish_cypher:
#---------#
	li, $t8, 0			#
	sb, $t8, 0($t1)			# zakonczenie szyfrogramu
	
	la $a0, a_cypher_ans		#
	la $v0, 4			#
	syscall				#
					# Wypisanie szyfrogramu
	la $a0, -448($fp)		#
	la $v0, 4			#
	syscall				#
	
	b finish_program
#-----#

#-----# DESZYFROWANIE
decypher:
	la $a0, a_decypher_ask		#
	li $v0, 4			# Wypisanie zaptytania o wiadomosc
	syscall				#
	
	addi $sp, $sp, -100		# CRYPTOGRAM: -148
	
	la $a0, -148($fp)		#
	li $a1, 100			# Wczytanie wiadomosci
	li $v0, 8			#
	syscall				#
	
	addi $sp, $sp, -100		# MESSAGE: -248
	
	la $t0, -148($fp)		# miejsce w szyfrogramie
	la $t1, -248($fp)		# miejsce we wiadomosci
	la $t2, -44($fp)		# adres poczatku szachownicy
	lw $t7, -48($fp)		# dlugosc klucza

	lb $t3, 0($t0)			#
	beq $a0, ' ', d_eliminate_space	# pominiecie spacji przed pierwszym znakiem w szyfrogramie

	beq $t3, '\n', finish_decypher	# sprawdzenie ko�ca szyfrogramu
decypher_loop:
	blt $t3, '0' error_in_cryptograph
	bgt $t3, '9' error_in_cryptograph
	subi $t3, $t3, '0'		# zamiana znaku na liczbe
#---------# DEKODOWANIE ZNAKU
	beqz $t3, d_outside_key		# sprawdzenie czy kod 1 czy 2 cyfrowy 
	ble $t3, $t7, d_in_key 		#
d_outside_key:
	addi $t3, $t3, 9		#
	rem $t3, $t3, 10		# zamiana 0 na 10
	addi $t3 $t3, 1			#
	sub $t3, $t3, $t7 		# zamiana liczby na numer wiersza przez odjecie dlugosci klucza
	lb $t4, 1($t0)			# wczytanie kolejnego znaku
	blt $t4, '0' error_in_cryptograph
	bgt $t4, '9' error_in_cryptograph
	subi $t4, $t4, '0'		# zamiana na cyfre
	addi $t4, $t4, 9		# zamiana cyfry na indeks we wierszu
	rem $t4, $t4, 10		#
	
	li $t5, 10			#
	mul $t5, $t5, $t3		# policzenie indeksu znaku w tablicy
	add $t5, $t5, $t4 		#
	blt $t5, 10 error_in_cryptograph
	bge $t5, 40 error_in_cryptograph
	
	add $t6, $t5, $t2		# odczytanie znaku z szachownicy
	lb $t6, 0($t6)			#
	beqz $t6, error_in_cryptograph
	
	sb $t6, 0($t1)			# zapisanie znaku we wiadomosci
	addi $t1, $t1, 1		# przesuniecie miejsca we wiadomosci
	addi $t0, $t0, 1		# przesuniecie dodatkowego miejsca w szyfrogramie
	
	addi $t0, $t0, 1		# przesuniecie miejsca w 
	lb $t3, 0($t0)			#
	beq $t3, ' ', d_eliminate_space
	bne $t3, '\n', decypher_loop	# sprawdzenie ko�ca szyfrogramu
	b finish_decypher
d_in_key:
	add $t4, $t2, $t3		# wczytanie znaku z szachownicy
	addi $t4, $t4, -1		#
	lb $t5, 0($t4)			#
	
	sb $t5, 0($t1)			# zapisanie znaku we wiadomosci
	addi $t1, $t1, 1		# przesuniecie miejsca we wiadomosci
	
d_eliminate_space:
	addi $t0, $t0, 1		# przesuniecie miejsca w 
	lb $t3, 0($t0)			#
	beq $t3, ' ', d_eliminate_space
	bne $t3, '\n', decypher_loop	# sprawdzenie ko�ca szyfrogramu

#---------#
finish_decypher:
	li, $t8, 0			#
	sb, $t8, 0($t1)			# zakonczenie wiadomosci
	
	la $a0, a_decypher_ans		#
	la $v0, 4			#
	syscall				#
					#
	la $a0, -248($fp)		#
	la $v0, 4			# Wypisanie szyfrogramu
	syscall				#
#-----#

#-#
	
finish_program:
	li $v0, 10			# koniec programu
	syscall				#

error_key_too_long:
	la $a0, m_err_key_len		# ustawienie wiadomosci bledu
	b finish_error

error_in_message:
	la $a0, m_err_in_msg		# ustawienie wiadomosci bledu
	b finish_error

error_in_cryptograph:
	la $a0, m_err_in_cry		# ustawienie wiadomosci bledu
	b finish_error

finish_error:
	li $v0, 4			# Wypisanie wiadomo?ci b??du
	syscall				#
	
	li $a0, 1			#
	li $v0, 17			# koniec programu z kodem b??du
	syscall				#
