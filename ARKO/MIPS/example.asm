	.data
text0:	.asciiz "Enter strng: \n"	# definicja stringu, który zostanie wypisany na konsoli
buf0:	.space 100					# definicja bufora na wczytywany string
	
	.text
	.globl main
	
main:
	la $a0, text0					# załadowanie adresu string`a 'text0', jako argument wywołania systemowego które wypisze string na konsoli
	li $v0, 4						# załadowanie numeru procedury systemowej, która zostanie uruchomiona - numer 4 odpowiada procedurze piszącej na konsoli
	syscall							# przekazanie sterowania do systemu operacyjnego, system operacyjny wypisze na konsoli string który znajduje się pod adresem załadowanym do rejestru a0
	
	la $a0, buf0					# załadowanie adresu bufora 'buf0', jako argument wywołania systemowego które wczyta string z konsoli
	li $a1, 100						# ile maksymalnie znaków system operacyjny może wczytać do bufora (pamiętamy, że string musi być zakończony wartością 0)
	li $v0, 8						# załadowanie numeru procedury systemowej - numer 8 odpowiada procedurze czytającej z konsoli
	syscall							# przekazanie sterowania do systemu operacyjnego, procedura ta zawiesi program dopuki użytkownik nie wpisze ciągu znaków
	
	li $t0, 'a'						# tutaj definiujemy kilka wartości które będą potrzebne później
	li $t1, 'z'						# 	wartości te muszą znajdować się w rejestrach, ponieważ instrukcje które z nich będą korzystać, nie mogą przyjąć jako argumentu wartości natychmiastowej (czyli wartości na sztywno zapisanej w programie - muszą to być wartości w rejestrach)
	li $t2, 0x20
	
	la $t3, buf0					# rejestr t3 będzie tymczasowym wskaźnikiem na bufor do którego został wczytany string
									# 	na początku działania programu, wskaźnik ten, wskazuje na początek ciągu - później będzie on sukcesywnie przesuwany aż do końca string`u
	
loop_begin:
	lb $t4, ($t3)					# do rejestru t4 ładujemy bajt, który jest zapisany w pamięci pod adresem zawartym w t3, w pierwszej iteracji pętli adres w t3 wskazuje na początek bufora, więc załadowana będzie pierwsza litera string`u
	beq $t4, $zero, loop_end		# jeżeli wczytany bajt jest równy zero (zawartość rejestru $zero zawsze wynosi 0) to skaczemy do punktu wyjścia z pętli (beq == branch if equal)
	
	blt $t4, $t0, increment_ptr		# jeżeli wartość bajtu jest przed 'a' w tablicy ASCII, to skaczemy do etykiety 'increment_ptr' (blt == branch if lower than)
	bgt $t4, $t1, increment_ptr		# jeżeli wartość bajtu jest za 'z' w tablicy ASCII, to skaczemy do etykiety 'increment_ptr' (blt == branch if greater than)
	
	sub $t4, $t4, $t2				# jeżeli znaleźliśmy się tutaj tzn. że wczytany bajt jest >= 'a' oraz <= 'z' - a to oznacza że jest on małą literą alfabetu greckiego; gdyodejmiemy od niego wartość 0x20 to zamiast małej litery otrzymamy odpowiadającą jej wielką literę
	sb $t4, ($t3)					# zamienioną wartość musimy jeszcze zapisać w pamięci tam skąd została ona wcześniej pobrana
	
increment_ptr:
	addi $t3, $t3, 1				# bieżący znak został przetworzony, idziemy do kolejnego znaku - wskaźnik na bufor przesuwamy o jeden bajt do przodu, teraz będzie przetwarzany kolejny wczytany znak
	b loop_begin					# skok do początku pętli (b == branch   bezwarunkowy skok)
loop_end:

	la $a0, buf0					# podobnie jak na początku programu ładujemy jako parametr wywołania adres bufora w którym znajduje się przetwarzany string
	li $v0, 4						# numer wywołania 4 odpowiada procedurze wypisującej wskazany string na konsoli
	syscall							# wywołanie procedury - przetwarzany string zostanie wypisany na konsoli
	
	li $v0, 10						# wywołanie systemowe numer 10 powoduje zakończenie działania programu
	syscall							# :-)
