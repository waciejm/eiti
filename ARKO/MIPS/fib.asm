	.data
text0:	.asciiz "Enter number: \n"
seperator: .asciiz ", "

	.text
	.globl main
	
main:
	# print text
	la $a0, text0
	li $v0, 4  # print string
	syscall
	
	# read int
	li $v0, 5  # read int
	syscall
	
	# check for zero
	ble $v0, $zero, loop_end
	
	# set counter to read value
	la $t0, ($v0)
	
	# set up registers with fib numbers
	li $t1, 0
	li $t2, 1
	
loop_begin:
	# calculate new fib numbers
	la $t3, ($t2)
	addu $t2, $t2, $t1
	la $t1, ($t3)
	
	# print next fib number
	la $a0, ($t1)
	li $v0, 36  # print unsigned
	syscall
	
	# print separator
	la $a0, seperator
	li $v0, 4  # print string
	syscall

decrement_counter:
	subi $t0, $t0, 1
	bgt $t0, $zero, loop_begin
	
loop_end:

	li $v0, 10
	syscall
	