// Maciej Wojno 2020
// git@mwojno.me

#pragma once
#include <algorithm>

template<typename Value>
struct Node {
  Value value;
  Node *left;
  Node *right;
  int height;
  Node(Value val) : value(val), left(nullptr), right(nullptr), height(1) {}
};

template<typename Value>
void update_height(Node<Value>* current) {
  if (current == nullptr) return;
  int h_left = current->left ? current->left->height : 0;
  int h_right = current->right ? current->right->height : 0;
  current->height = std::max(h_left, h_right) + 1;
}

template<typename Value>
int balance_factor(Node<Value>* current) {
  if (current == nullptr) return 0;
  int h_left = current->left ? current->left->height : 0;
  int h_right = current->right ? current->right->height : 0;
  return h_right - h_left;
}

template<typename Value>
void rotate_right(Node<Value>*& current) {
  Node<Value>* left = current->left;
  current->left = left->right;
  left->right = current;
  current = left;
  update_height(current->right);
  update_height(current);
}

template<typename Value>
void rotate_left(Node<Value>*& current) {
  Node<Value>* right = current->right;
  current->right = right->left;
  right->left = current;
  current = right;
  update_height(current->left);
  update_height(current);
}

template<typename Value>
void balance(Node<Value>*& current) {
  int bf = balance_factor(current);
  if (bf < -1) {
    if (balance_factor(current->left) < 0) {
      rotate_right(current);
    } else {
      rotate_left(current->left);
      rotate_right(current);
    }
  } else if (bf > 1) {
    if (balance_factor(current->right) < 0) {
      rotate_right(current->right);
      rotate_left(current);
    } else {
      rotate_left(current);
    }
  }
}

template<typename Value>
Node<Value> *take_lowest(Node<Value>*& current) {
  if (current == nullptr) return nullptr;
  if (current->left == nullptr) {
    Node<Value>* tmp = current;
    current = current->right;
    return tmp;
  } else {
    Node<Value>* tmp = take_lowest(current->left);
    balance(current);
    update_height(current);
    return tmp;
  }
}

template<typename Value>
Node<Value> *take_highest(Node<Value>*& current) {
  if (current == nullptr) return nullptr;
  if (current->right == nullptr) {
    Node<Value>* tmp = current;
    current = current->left;
    return tmp;
  } else {
    Node<Value>* tmp = take_highest(current->right);
    balance(current);
    update_height(current);
    return tmp;
  }
}

template<typename Value>
void print_tree(Node<Value>* current, int level) {
  if (current == nullptr) return;
  print_tree(current->left, level + 1);
  std::cout << current->value << " " << current -> height << " " << level << std::endl;
  print_tree(current->right, level + 1);
}

template<typename Value>
void insert(Node<Value>*& current, Value value) {
  if (current == nullptr) {
    current = new Node<Value>(value);
  } else {
    if (value < current->value) {
      insert(current->left, value);
    } else if (value > current-> value) {
      insert(current->right, value);
    }
    balance(current);
    update_height(current);
  }
}

template<typename Value>
void remove(Node<Value>*& current, Value value) {
  if (current == nullptr) return;
  if (value == current->value) {
    Node<Value>* newcurr;
    if (balance_factor(current) <= 0) {
      newcurr = take_highest(current->left);
    } else {
      newcurr = take_lowest(current->right);
    }
    if (newcurr != nullptr) {
      newcurr->right = current->right;
      newcurr->left = current->left;
    }
    Node<Value>* tmp = current;
    current = newcurr;
    delete(tmp);
  } else if (value < current->value) {
    remove(current->left, value);
  } else {// value > current->value
    remove(current->right, value);
  }
  balance(current);
  update_height(current);
}