#include <iostream>
#include <vector>

template <typename T, typename Collection, typename Iterator>
class MyCollection : public Collection
{
public:
	class MyIterator
	{
		friend MyCollection;
	private:
		Collection & collection;
		Iterator current;
		bool finished;

		MyIterator(Collection & col, Iterator const i) : collection(col), finished( !(col.begin() != col.end()) ) {}

	public:

		T & operator*()
		{
			return *current;
		}

		MyIterator & operator++()
		{
			Iterator next;
			bool has_next = false;
			Iterator start = current;
			++start;
			// Szukanie takich samych albo większych wartości po obecnym elemencie
			for (Iterator i = start; i != collection.end(); ++i) {
				if (has_next) {
					if (*i < *next && *i >= *current) {
						next = i;
					}
				} else {
					if (*i >= *current) {
						next = i;
						has_next = true;
					}
				}
			}
			// Szukanie wiekszych wartosci przed obecnym elementem
			// jeśli nie zleziono wczesniej dalszego równego
			if (!has_next || *next != *current) {
				for (Iterator i = collection.begin(); i != current; ++i) {
					if (has_next) {
						if (*i < *next && *i > *current) {
							next = i;
						}
					} else {
						if (*i > *current) {
							next = i;
							has_next = true;
						}
					}
				}
			}
			// Koniec iteratora jeśli nie znaleziono kolejnego elementu
			if (!has_next) {
				finished = true;
			} else {
				current = next;
			}
			return *this;
		}

		bool operator!=(MyIterator const& i) const
		{
			if (finished && i.finished) {
				return false;
			} else if ((finished && !i.finished) || (!finished && i.finished)) {
				return true;
			} else {
				return current != i.current;
			}
		}
	};

	MyIterator myBegin()
	{
		MyIterator mi = MyIterator(*this, this->begin());
		if (!mi.finished) {
			mi.current = this->begin();
			for (Iterator i = ++this->begin(); i != this->end(); ++i) {
				if (*i < *mi.current) {
					mi.current = i;
				}
			}
		}
		return mi;
	}

	MyIterator myEnd()
	{
		MyIterator mi = MyIterator(*this, this->end());
		mi.finished = true;
		return mi;
	}
};


int main()
{
	MyCollection<int, std::vector<int>, std::vector<int>::iterator> col;
	
	for (int i = 0; i < 100; ++i)
		col.push_back( rand() % 20 );

	std::cout << "Vector (standard iterator):" << std::endl;
	for( auto const i: col)
		std::cout << i << ", ";
	std::cout << std::endl;

	std::cout << "Vector (my iterator):" << std::endl;
	for (auto i = col.myBegin(); i != col.myEnd(); ++i)
		std::cout << *i << ", ";
	std::cout << std::endl;
}

