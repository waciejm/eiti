#include "demo.hpp"

int main() {
    #ifdef _DEBUG
        return test_it_all();
    #else
        return demo();
    #endif
}