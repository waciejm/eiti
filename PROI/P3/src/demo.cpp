#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <memory>
#include <functional>
#include "question.hpp"
#include "demo.hpp"

void show_vehicles(std::vector<std::unique_ptr<Vehicle>> &);
void make_action(std::vector<std::unique_ptr<Vehicle>> &, const std::string, bool &);

int demo() {
    const std::string filename = "vehicles.txt";

    bool end = false;

    std::vector<std::unique_ptr<Vehicle>> vehicles;

    while (!end) {
        show_vehicles(vehicles);
        make_action(vehicles, filename, end);
        std::stable_sort(vehicles.rbegin(), vehicles.rend(),
            [&](const std::unique_ptr<Vehicle> &lhs, const std::unique_ptr<Vehicle> &rhs){
                return *lhs < *rhs;
            });
    }

    return 0;
}

void show_vehicles(std::vector<std::unique_ptr<Vehicle>> &arr) {
    std::cout << std::endl;
    for (unsigned int i = 0; i < arr.size(); ++i) {
        std::cout << i + 1 << "\t" << *arr[i] << std::endl;
    }
    std::cout << std::endl;
}

void make_action(std::vector<std::unique_ptr<Vehicle>> &arr, const std::string filename, bool &end) {
    std::string tmp;
    std::ofstream outf;
    std::ifstream inf;

    bool exit = false;
    int choice;

    switch (question::get_int_in_range("1. Choose vehicle  2. Add vehicle  3. Save to file  4. Load from file  5. Exit", 1, 6)) {
    case 1:
        // CHOOSE VEHICLE
        if (arr.size() > 0) {
            choice = -1 + question::get_int_in_range("Enter vehicle id:", 1, arr.size() + 1);
            // WORKING ON CHOSEN VEHICLE
            while (!exit) {
                std::cout << std::endl << *arr[choice] << std::endl;
                switch (question::get_int_in_range("1. Ride  2. Edit  3. Delete  4. Exit", 1, 5)) {
                case 1:
                    if (arr[choice]->can_ride()) {
                        arr[choice]->ride(question::get_float("Distance to ride: "));
                    } else {
                        std::cout << "This vehicle can't ride\n";
                    }
                break;
                case 2:
                    arr[choice]->setup();
                break;
                case 3:
                    arr.erase(arr.begin() + choice);
                    exit = true;
                break;
                case 4:
                    exit = true;
                }
            }
        } else {
            std::cout << " No vehicles to choose from\n";
        }
    break;
    case 2:
        // ADD VEHICLE
        switch (question::get_int_in_range("1. Bike  2. PremiumBike  3. Car", 1, 4)) {
        case 1:
            arr.push_back(std::unique_ptr<Vehicle>(new Bike(question::get_string("Bike name: "))));
        break;
        case 2:
            arr.push_back(std::unique_ptr<Vehicle>(new PremiumBike(question::get_string("PremiumBike name: "))));
        break;
        case 3:
            arr.push_back(std::unique_ptr<Vehicle>(new Car(question::get_string("Car name: "))));
        }
    break;
    case 3:
        // SAVE TO FILE
        outf.open(filename);
        for (auto &vh : arr) {
            outf << *vh << std::endl;
        }
        outf.close();
    break;
    case 4:
        // LOAD FROM FILE
        inf.open(filename);
        arr.clear();
            inf >> tmp;
        while (!inf.eof()) {
            if (tmp == "BIKE") {
                arr.push_back(std::unique_ptr<Vehicle>(new Bike()));
            } else if (tmp == "PREMIUMBIKE") {
                arr.push_back(std::unique_ptr<Vehicle>(new PremiumBike()));
            } else if (tmp == "CAR") {
                arr.push_back(std::unique_ptr<Vehicle>(new Car()));
            }
            inf >> *arr.back();
            inf >> tmp;
        }
        inf.close();
    break;
    case 5:
        end = true;
    }
}

int test_it_all() {
    std::cout << "TESTING...\n";
    // Counter
    {
        Counter counter;
        assert(counter.get_count() == 0.0);
        counter.increase(1.0);
        assert(counter.get_count() == 1.0);
        counter += 1.0;
        assert(counter.get_count() == 2.0);
        assert(counter.get_count() == 2.0);
        std::cout << "Below you should see 2\n" << counter << std::endl;
        Counter counter2;
        assert(counter > counter2);
        assert(!(counter < counter2));
        assert(!(counter == counter2));
        counter2 += 2.0;
        assert(counter == counter2);
        counter2 += 1.0;
        assert(counter < counter2);
        Counter counter3(counter);
        assert(counter == counter3);
        assert(&counter != &counter3);
        Counter counter4;
        counter4 = counter2;
        assert(counter4 == counter2);
    }

    // Ringer
    {
        Ringer ringer;
        assert(ringer.ring() == "Ring!");
        //std::cout << "Below you should see Ring!\n" << ringer << std::endl;
        Ringer ringer2("Reeee");
        assert(ringer2.ring() == "Reeee");
        //std::cout << "Below you should see Reeee\n" << ringer2 << std::endl;
        std::string string = ringer2;
        assert(string == ringer2);
        assert(ringer2 == string);
        Ringer ringer3(ringer);
        assert(ringer3 == ringer);
        assert(&ringer != &ringer3);
        Ringer ringer4;
        ringer4 = ringer2;
        assert(ringer4 == ringer2);
    }

    // Wheel
    {
        Wheel wheel(1.0);
        assert(wheel.get_rotation() == 0.0);
        assert(wheel.get_diameter() == 1.0);
        wheel.roll(1.0);
        assert(wheel.get_rotation() == 2.0);
        wheel.roll(-0.5);
        assert(wheel.get_rotation() == 1.0);
        wheel.roll(-0.51);
        assert(wheel.get_rotation() > 6.0);
        std::cout << "You should see an invalid argument exception below\n";
        try {
            Wheel(-1);
        } catch (const std::invalid_argument &ia) {
            std::cout << "Invalid argument: " << ia.what() << std::endl;
        }
        Wheel wheel2(wheel);
        assert(wheel2.get_diameter() == wheel.get_diameter());
        assert(wheel2.get_rotation() == wheel.get_rotation());
        assert(&wheel2 != &wheel);
        Wheel wheel3(10.0);
        assert(wheel3.get_diameter() != wheel2.get_diameter());
        wheel3 = wheel2;
        assert(wheel3.get_diameter() == wheel2.get_diameter());

    }

    // Rack
    {
        Rack rack = Rack();
        assert(rack.look_spare() == nullptr);
        Wheel *wheel = new Wheel(2.0);
        assert(rack.add_spare(wheel));
        assert(!rack.add_spare(wheel));
        assert(rack.look_spare() == wheel);
        assert(rack.take_spare() == wheel);
        assert(rack.take_spare() == nullptr);
        assert(rack.look_spare() == nullptr);
        assert(rack.add_spare(wheel));
        Rack rack2(rack);
        assert(rack.look_spare() != rack2.look_spare());
        assert(rack.look_spare()->get_diameter() == rack2.look_spare()->get_diameter());
        Rack rack3;
        rack3 = rack2;
        assert(rack3.look_spare() != rack2.look_spare());
        assert(rack3.look_spare()->get_diameter() == rack2.look_spare()->get_diameter());
    }

    // Frame
    {
        Wheel *wheel0 = new Wheel(3.0);
        Wheel *wheel1 = new Wheel(4.0);
        Wheel *wheel2 = new Wheel(5.0);
        Rack *rack = new Rack();
        std::cout << "You should see an invalid argument exception below\n";
        try {
            Frame frame = Frame(wheel0, wheel0);
        } catch (const std::invalid_argument &ia) {
            std::cout << ia.what() << std::endl;
        }
        Frame frame = Frame(wheel0, wheel1, rack);
        assert(frame.look_wheel(0) == wheel0);
        assert(frame[0] == wheel0);
        assert(frame.look_wheel(1) == wheel1);
        assert(frame[1] == wheel1);
        assert(frame.look_rack() == rack);
        assert(frame.take_wheel(0) == wheel0);
        assert(frame[0] == nullptr);
        assert(frame.take_wheel(0) == nullptr);
        assert(frame.take_wheel(1) == wheel1);
        assert(frame[1] == nullptr);
        assert(frame.take_wheel(1) == nullptr);
        assert(frame.take_rack() == rack);
        assert(frame.look_rack() == nullptr);
        assert(frame.take_rack() == nullptr);
        assert(!frame.swap_with_spare(0));
        assert(frame.add_rack(rack));
        assert(frame.add_spare(wheel2));
        assert(frame.look_spare() == wheel2);
        assert(frame.take_spare() == wheel2);
        assert(frame.look_spare() == nullptr);
        assert(!frame.roll_wheel(0, 1.0));
        assert(!frame.roll_wheel(1, 1.0));
        assert(frame.add_wheel(0, wheel0));
        assert(frame.add_wheel(1, wheel1));
        assert(frame.add_spare(wheel2));
        assert(frame.roll_wheel(0, 1.0));
        assert(frame[0]->get_rotation() > 0.0);
        assert(frame[1]->get_rotation() == 0.0);
        frame.swap_wheels();
        assert(frame[0] == wheel1);
        assert(frame[1] == wheel0);
        assert(frame.swap_with_spare(0));
        assert(frame[0] == wheel2);
        assert(frame.look_spare() == wheel1);
        Frame frame2(frame);
        Frame frame3;
        frame3 = frame2;
        assert(frame3.look_wheel(0) != frame2.look_wheel(0));
        assert(frame3.look_spare() != frame2.look_spare());
        assert(frame3.look_wheel(1)->get_diameter() == frame2.look_wheel(1)->get_diameter());
    }

    // PremiumBike
    {
        PremiumBike bike = PremiumBike();
        assert(!bike.can_ride());
        std::cout << "You should see an invalid ride exception below\n";
        try {
            bike.ride(1.0);
        } catch (invalid_ride &ir) {
            std::cout << ir.what() << std::endl;
        }
        assert(!bike.has_counter());
        assert(!bike.has_ringer());
        assert(!bike.has_front_wheel());
        assert(!bike.has_back_wheel());
        assert(!bike.has_rack());
        assert(!bike.has_spare());
        Ringer *ringer = new Ringer("ringer");
        Counter *counter = new Counter();
        Wheel *wheel0 = new Wheel(1.0);
        Wheel *wheel1 = new Wheel(2.0);
        Wheel *wheel2 = new Wheel(3.0);
        Rack *rack = new Rack();
        assert(bike.add_counter(counter));
        assert(bike.ring() == "");
        assert(!bike.ring(std::cout));
        assert(bike.add_ringer(ringer));
        assert(bike.ring() == "ringer");
        assert(bike.add_front_wheel(wheel0));
        assert(bike.add_back_wheel(wheel1));
        assert(!bike.add_spare(wheel2));
        assert(bike.add_rack(rack));
        assert(bike.add_spare(wheel2));
        assert(bike.look_counter() == counter);
        assert(bike.look_ringer() == ringer);
        assert(bike.look_front_wheel() == wheel0);
        assert(bike.look_back_wheel() == wheel1);
        assert(bike.look_rack() == rack);
        assert(bike.look_spare() == wheel2);
        bike.delete_counter();
        assert(!bike.has_counter());
        bike.delete_ringer();
        assert(!bike.has_ringer());
        bike.delete_front_wheel();
        assert(!bike.has_front_wheel());
        bike.delete_back_wheel();
        assert(!bike.has_back_wheel());
        assert(bike.delete_spare());
        assert(!bike.has_spare());
        bike.delete_rack();
        assert(!bike.has_rack());
        assert(bike.new_counter());
        assert(!bike.new_counter());
        assert(bike.has_counter());
        assert(bike.new_ringer("ring"));
        assert(!bike.new_ringer());
        assert(bike.has_ringer());
        assert(bike.ring() == "ring");
        std::cout << "Below you should see ring\n";
        assert(bike.ring(std::cout));
        std::cout << std::endl;
        assert(bike.new_front_wheel(4.0));
        assert(!bike.new_front_wheel(1.0));
        assert(bike.has_front_wheel());
        assert(bike.new_back_wheel(5.0));
        assert(!bike.new_back_wheel(1.0));
        assert(bike.has_front_wheel());
        assert(!bike.new_spare(1.0));
        assert(bike.new_rack());
        assert(!bike.new_rack());
        assert(bike.has_rack());
        assert(!bike.has_spare());
        assert(bike.new_spare(6.0));
        assert(!bike.new_spare(1.0));
        assert(bike.has_spare());
        assert(bike.look_front_wheel()->get_diameter() == 4.0);
        assert(bike.look_back_wheel()->get_diameter() == 5.0);
        bike.swap_wheels();
        assert(bike.look_back_wheel()->get_diameter() == 4.0);
        assert(bike.look_front_wheel()->get_diameter() == 5.0);
        bike.swap_wheels();
        assert(bike.swap_front_spare());
        assert(bike.look_front_wheel()->get_diameter() == 6.0);
        assert(bike.look_spare()->get_diameter() == 4.0);
        assert(bike.swap_front_spare());
        assert(bike.swap_back_spare());
        assert(bike.look_back_wheel()->get_diameter() == 6.0);
        assert(bike.look_spare()->get_diameter() == 5.0);
        assert(bike.swap_back_spare());
        assert(bike.get_count() == 0.0);
        assert(bike.can_ride());
        std::cout << "You should NOT see an invalid ride exception below\n";
        try {
            bike.ride(1.0);
        } catch (invalid_ride &ir) {
            std::cout << ir.what() << std::endl;
        }
        assert(bike.get_count() == 1.0);
        assert(bike.look_front_wheel()->get_rotation() > 0.0);
        assert(bike.look_back_wheel()->get_rotation() > 0.0);
        assert(bike.look_spare()->get_rotation() == 0.0);
        PremiumBike bike2(bike);
        PremiumBike bike3;
        bike3 = bike2;
        std::cout << bike3 << std::endl;
        assert(bike3.look_front_wheel() != bike2.look_front_wheel());
    }

    // Car
    {
        // TODO
    }

    std::cout << "END TESTING\n";
    return 0;
}