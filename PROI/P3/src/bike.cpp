#include <functional>
#include <stdexcept>
#include "question.hpp"
#include "bike.hpp"

Bike & Bike::operator=(const Bike &bike) {
    if (this == &bike) return *this;
    frame = bike.frame;
    return *this;
}

void Bike::ride(float distance) {
    if (can_ride()) {
        if (has_front_wheel()) frame.roll_wheel(0, distance);
        if (has_back_wheel()) frame.roll_wheel(1, distance);
    } else {
        throw(invalid_ride("Runtime error: Tried to ride a Bike that has no wheels!"));
    }
}

bool Bike::can_ride() const {
    return has_front_wheel() || has_back_wheel();
}

void Bike::setup() {
    if (question::get_answer("Modify front wheel?", "y", "n")) {
        delete frame.take_wheel(0);
        if (question::get_answer("Front wheel on bike?", "y", "n")) {
            frame.add_wheel(0, new Wheel(question::get_float_with_condition("Wheel diameter: ",
                           [&](float f){ if (f > 0) return true; else return false; })));
        }
    }
    if (question::get_answer("Modify back wheel?", "y", "n")) {
        delete frame.take_wheel(1);
        if (question::get_answer("Back wheel on bike?", "y", "n")) {
            frame.add_wheel(1, new Wheel(question::get_float_with_condition("Wheel diameter: ",
                           [&](float f){ if (f > 0) return true; else return false; })));
        }
    }
}

void Bike::write(std::ostream &out) const {
    out << "BIKE ";
    if (name != "") {
        out << name;
    } else {
        out << "~NAME";
    }
    out << " FRAME ";
    frame.write(out);
}

void Bike::read(std::istream &in) {
    std::string tmp;
    in >> tmp;
    if (tmp == "~NAME") {
        name = "";
    } else {
        name = tmp;
    }
    in >> tmp;
    frame.read(in);
}

void Bike::delete_front_wheel() {
    if (has_front_wheel()) delete frame.take_wheel(0);
}

void Bike::delete_back_wheel() {
    if (has_back_wheel()) delete frame.take_wheel(1);
}

bool Bike::new_front_wheel(float d) {
    if (has_front_wheel()) return false;
    Wheel *tmp = new Wheel(d);
    if (!add_front_wheel(tmp)) {
        delete tmp;
        return false;
    }
    return true;
}

bool Bike::new_back_wheel(float d) {
    if (has_back_wheel()) return false;
    Wheel *tmp = new Wheel(d);
    if (!add_back_wheel(tmp)) {
        delete tmp;
        return false;
    }
    return true;
}

void Bike::swap_wheels() {
    return frame.swap_wheels();
}