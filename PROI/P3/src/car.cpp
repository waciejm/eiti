#include <functional>
#include <stdexcept>
#include "car.hpp"
#include "question.hpp"

Car::Car(const Car &car) : horn("Honk!"), counter(car.counter) {
    for (int i = 0; i < 4; ++i) {
        if (car.wheel[i] != nullptr) wheel[i] = new Wheel((*car.wheel[i]));
    }
}

Car::~Car() {
    for (int i = 0; i < 4; ++i) {
        if (wheel[i] != nullptr) delete wheel[i];
    }
}

Car & Car::operator=(const Car &car) {
    for (int i = 0; i < 4; ++i) {
        if (wheel[i] == nullptr) delete wheel[i];
        if (car.wheel[i] != nullptr)
            wheel[i] = new Wheel(*(car.wheel[i]));
        else
            wheel[i] = nullptr;
    }
    counter = car.counter;
    return *this;
}

void Car::ride(float dist) {
    if (can_ride()) {
        for (int i = 0; i < 4; ++i) {
            wheel[i]->roll(dist);
        }
        counter += dist;
    } else {
        throw(invalid_ride("Runtime error: Tried to drive a Car that doesn't have all wheels!"));
    }
}

bool Car::can_ride() const {
    for (int i = 0; i < 4; ++i) {
        if (wheel[i] == nullptr) return false;
    }
    return true;
}

void Car::setup() {
    if (question::get_answer("Modify first wheel?", "y", "n")) {
        delete_wheel(0);
        if (question::get_answer("First wheel on car?", "y", "n")) {
            new_wheel(0, question::get_float_with_condition("Wheel diameter: ",
                     [&](float f){ if (f > 0) return true; else return false; }));
        }
    }
    if (question::get_answer("Modify second wheel?", "y", "n")) {
        delete_wheel(1);
        if (question::get_answer("Second wheel on car?", "y", "n")) {
            new_wheel(1, question::get_float_with_condition("Wheel diameter: ",
                     [&](float f){ if (f > 0) return true; else return false; }));
        }
    }
    if (question::get_answer("Modify third wheel?", "y", "n")) {
        delete_wheel(2);
        if (question::get_answer("Third wheel on car?", "y", "n")) {
            new_wheel(2, question::get_float_with_condition("Wheel diameter: ",
                     [&](float f){ if (f > 0) return true; else return false; }));
        }
    }
    if (question::get_answer("Modify fourth wheel?", "y", "n")) {
        delete_wheel(3);
        if (question::get_answer("Fourth wheel on car?", "y", "n")) {
            new_wheel(3, question::get_float_with_condition("Wheel diameter: ",
                     [&](float f){ if (f > 0) return true; else return false; }));
        }
    }
    if (question::get_answer("Reset counter?", "y", "n")) {
        counter.reset();
    }
}

void Car::write(std::ostream &out) const {
    out << "CAR ";
    if (name != "") {
        out << name;
    } else {
        out << "~NAME";
    }
    out << " ";
    for (int i = 0; i < 4; ++i) {
        if (wheel[i] == nullptr) {
            out << "~WHEEL ";
        } else {
            out << "WHEEL ";
            wheel[i]->write(out);
        }
    }
    out << "HORN ";
    horn.write(out);
    out << "COUNTER ";
    counter.write(out);
}

void Car::read(std::istream &in) {
    std::string tmp;
    in >> tmp;
    if (tmp == "~NAME") {
        name = "";
    } else {
        name = tmp;
    }
    for (int i = 0; i < 4; ++i) {
        if (wheel[i] != nullptr) delete wheel[i];
        in >> tmp;
        if (tmp == "~WHEEL") {
            wheel[i] = nullptr;
        } else {
            wheel[i] = new Wheel(1.0);
            wheel[i]->read(in);
        }
    }
    in >> tmp;
    horn.read(in);
    in >> tmp;
    counter.read(in);
}

const Wheel * Car::look_wheel(int id) const {
    if (id < 0 || id > 3) return nullptr;
    return wheel[id];
}

bool Car::new_wheel(int id, float d) {
    if (id < 0 || id > 3 || d <= 0 || wheel[id] != nullptr) return false;
    wheel[id] = new Wheel(d);
    return true;
}

void Car::delete_wheel(int id) {
    if (wheel[id] != nullptr) {
        delete wheel[id];
        wheel[id] = nullptr;
    }
}

const std::string & Car::honk() const {
    return horn.ring();
}

bool Car::honk(std::ostream &out) const {
    out << horn.ring();
    return true;
}

float Car::get_count() const {
    return counter.get_count();
}