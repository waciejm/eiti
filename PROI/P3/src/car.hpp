#pragma once
#include <ostream>
#include <string>
#include "vehicle.hpp"
#include "wheel.hpp"
#include "counter.hpp"
#include "ringer.hpp"

class Car : public Vehicle {
public:
    Car() : name(""), wheel{}, horn("Honk!") {}
    Car(std::string nm) : name(nm), wheel{}, horn("Honk!") {}
    Car(const Car &);
    virtual ~Car();

    Car & operator=(const Car &);

    virtual void ride(float);
    virtual bool can_ride() const;
    virtual void setup();
    virtual void write(std::ostream &) const;
    virtual void read(std::istream &);
    virtual const std::string & get_name() const { return name; }
    virtual float get_count() const;

    const Wheel * look_wheel(int) const;
    bool new_wheel(int, float);
    void delete_wheel(int);

    const std::string & honk() const;
    bool honk(std::ostream &) const;

protected:
    std::string name;
    Wheel *wheel[4];
    Ringer horn;
    Counter counter;
};