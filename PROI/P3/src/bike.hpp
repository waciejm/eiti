#pragma once
#include <iostream>
#include "vehicle.hpp"
#include "frame.hpp"

class Bike : public Vehicle {
public:
    Bike() : name(""), frame() {}
    Bike(std::string nm) : name(nm), frame() {}
    Bike(const Bike &bike) : frame(bike.frame) {}
    virtual ~Bike() = default;

    Bike & operator=(const Bike &);

    virtual void ride(float);
    virtual bool can_ride() const;
    virtual void setup();
    virtual void write(std::ostream &) const;
    virtual void read(std::istream &);
    virtual const std::string & get_name() const { return name; }
    virtual float get_count() const { return 0.0; }

    bool has_front_wheel() const { return (look_front_wheel() != nullptr); }
    bool has_back_wheel() const { return (look_back_wheel() != nullptr); }

    const Wheel * look_front_wheel() const { return frame.look_wheel(0); }
    const Wheel * look_back_wheel() const { return frame.look_wheel(1); }

    bool add_front_wheel(Wheel *whl) { return frame.add_wheel(0, whl); }
    bool add_back_wheel(Wheel *whl) { return frame.add_wheel(1, whl); }

    void delete_front_wheel();
    void delete_back_wheel();

    bool new_front_wheel(float);
    bool new_back_wheel(float);

    void swap_wheels();

protected:
    std::string name;
    Frame frame;
};