#pragma once
#include <string>
#include <iostream>
#include <exception>

class Vehicle {
public:
    virtual ~Vehicle() = default;

    virtual void ride(float) = 0;
    virtual bool can_ride() const = 0;
    virtual void setup() = 0;
    virtual void write(std::ostream &) const = 0;
    virtual void read(std::istream &) = 0;
    virtual const std::string & get_name() const = 0;
    virtual float get_count() const = 0;

    friend bool operator<(Vehicle &lhs, Vehicle &rhs) {
        if (lhs.get_count() != rhs.get_count()) {
            return lhs.get_count() < rhs.get_count();
        } else {
            return lhs.get_name() > rhs.get_name();
        }
    }
};

std::ostream & operator<<(std::ostream &, const Vehicle &);
std::istream & operator>>(std::istream &, Vehicle &);


class invalid_ride : std::exception {
public:
    invalid_ride(std::string msg) : _what(msg) {};
    const char * what() const noexcept { return _what.c_str(); }

protected:
    std::string _what;
};