#pragma once
#include <iostream>
#include "wheel.hpp"

class Rack {
    public:
        Rack() : spare(nullptr) {}
        Rack(const Rack &);
        ~Rack();

        Rack & operator=(const Rack &);
        void write(std::ostream &) const;
        void read(std::istream &);

        bool add_spare(Wheel *);
        Wheel * take_spare();
        const Wheel * look_spare() const;

        operator const Wheel*() const { return spare; }
        bool operator+=(Wheel *wheel) { return add_spare(wheel); }

    protected:
        Wheel *spare;
};

std::ostream & operator<<(std::ostream &, const Rack &);
std::istream & operator>>(std::istream &, Rack &);