#include <iostream>
#include "question.hpp"

bool question::get_answer(std::string question, std::string accept, std::string refuse) {
    std::cout << question << " (" << accept << "/" << refuse << ")" << std::endl;
    std::string tmp;
    std::cin >> tmp;
    if (tmp == accept) return true;
    if (tmp == refuse) return false;
    return get_answer(question, accept, refuse);
}

float question::get_float_with_condition(std::string question, std::function<bool (float)> condition) {
    std::cout << question << std::endl;
    std::string tmp;
    float f;
    std::cin >> tmp;
    try {
        f = std::stof(tmp);
    } catch (const std::invalid_argument &ia) {
        return get_float_with_condition(question, condition);
    }
    if (condition(f)) {
        return f;
    } else {
        return get_float_with_condition(question, condition);
    }
}

float question::get_float(std::string question) {
    std::cout << question << std::endl;
    std::string tmp;
    float f;
    std::cin >> tmp;
    try {
        f = std::stof(tmp);
    } catch (const std::invalid_argument &ia) {
        return get_float(question);
    }
    return f;
}

std::string question::get_string(std::string question) {
    std::cout << question << std::endl;
    std::string tmp;
    std::cin >> tmp;
    return tmp;
}

int question::get_int_with_condition(std::string question, std::function<bool (int)> condition) {
    std::cout << question << std::endl;
    std::string tmp;
    int a;
    std::cin >> tmp;
    try {
        a = std::stoi(tmp);
    } catch (const std::invalid_argument &ia) {
        return get_int_with_condition(question, condition);
    }
    if (condition(a)) {
        return a;
    } else {
        return get_int_with_condition(question, condition);
    }
}


int question::get_int_in_range(std::string question, int low, int high) {
    std::cout << question << std::endl;
    std::string tmp;
    int a;
    std::cin >> tmp;
    try {
        a = std::stoi(tmp);
    } catch (const std::invalid_argument &ia) {
        return get_int_in_range(question, low, high);
    }
    if (a >= low && a < high) {
        return a;
    } else {
        return get_int_in_range(question, low, high);
    }
}