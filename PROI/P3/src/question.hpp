#pragma once
#include <functional>
#include <string>
#include <cmath>

namespace question {
    bool get_answer(std::string, std::string, std::string);
    float get_float_with_condition(std::string, std::function<bool (float)>);
    float get_float(std::string);
    int get_int_with_condition(std::string, std::function<bool (int)>);
    int get_int_in_range(std::string, int, int);
    std::string get_string(std::string);
}