#pragma once
#include <stdexcept>
#include <iostream>

class Wheel {
    public:
        Wheel(float);
        Wheel(const Wheel &) = default;

        Wheel & operator=(const Wheel &) = default;
        void write(std::ostream &) const;
        void read(std::istream &);

        float get_diameter() const;
        float get_rotation() const;

        void roll(float);

    protected:
        float diameter;
        float rotation = 0.0; // Radians
};

std::ostream & operator<<(std::ostream &, const Wheel &);
std::istream & operator>>(std::istream &, Wheel &);