#pragma once
#include <iostream>

class Counter {
    public:
        Counter() : count(0.0) {}
        Counter(float cnt) : count(cnt) {}
        Counter(const Counter &) = default;

        Counter & operator=(const Counter &) = default;
        void write(std::ostream &) const;
        void read(std::istream &);

        void increase(float);
        void operator+=(float inc) { return increase(inc); }
        float get_count() const;
        void reset() { count = 0; }

        operator float() const { return count; }
        bool operator==(const Counter &) const;
        bool operator>(const Counter &) const;
        bool operator<(const Counter &) const;

    protected:
        float count;
};

std::ostream & operator<<(std::ostream &, const Counter &);
std::istream & operator>>(std::istream &, Counter &);