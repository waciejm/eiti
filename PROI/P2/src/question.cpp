#include <iostream>
#include "question.hpp"

bool question::get_answer(std::string question, std::string accept, std::string refuse) {
    std::cout << question << std::endl;
    std::string tmp;
    std::cin >> tmp;
    if (tmp == accept) return true;
    if (tmp == refuse) return false;
    return get_answer(question, accept, refuse);
}

float question::get_float(std::string question, std::function<bool (float)> condition) {
    std::cout << question << std::endl;
    std::string tmp;
    float f;
    std::cin >> tmp;
    try {
        f = std::stof(tmp);
    } catch (const std::invalid_argument &ia) {
        return get_float(question, condition);
    }
    if (condition(f)) {
        return f;
    } else {
        return get_float(question, condition);
    }
}

std::string question::get_string(std::string question) {
    std::cout << question << std::endl;
    std::string tmp;
    std::cin >> tmp;
    return tmp;
}

int question::get_int(std::string question, std::function<bool (int)> condition) {
    std::cout << question << std::endl;
    std::string tmp;
    int a;
    std::cin >> tmp;
    try {
        a = std::stoi(tmp);
    } catch (const std::invalid_argument &ia) {
        return get_int(question, condition);
    }
    if (condition(a)) {
        return a;
    } else {
        return get_int(question, condition);
    }
}

int question::triple(std::string question, std::string fst, std::string snd, std::string trd) {
    std::string tmp = question::get_string(question);
    if (tmp == fst) return 0;
    if (tmp == snd) return 1;
    if (tmp == trd) return 2;
    return triple(question, fst, snd, trd);
}