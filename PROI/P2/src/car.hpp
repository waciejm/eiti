#pragma once
#include <ostream>
#include <string>
#include "vehicle.hpp"
#include "wheel.hpp"
#include "counter.hpp"
#include "ringer.hpp"

class Car : public Vehicle {
public:
    Car() : wheel{}, horn("Honk!") {}
    Car(const Car &);
    virtual ~Car();

    Car & operator=(const Car &);

    virtual bool ride(float);
    virtual void setup();
    virtual void write(std::ostream &) const;
    virtual void read(std::istream &);

    const Wheel * look_wheel(int) const;
    bool new_wheel(int, float);
    void delete_wheel(int);

    const std::string & honk() const;
    bool honk(std::ostream &) const;
    float get_count() const;

protected:
    Wheel *wheel[4];
    Ringer horn;
    Counter counter;
};