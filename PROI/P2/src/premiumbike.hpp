#pragma once
#include "bike.hpp"
#include "counter.hpp"
#include "ringer.hpp"
#include <iostream>

class PremiumBike : public Bike {
public:
    PremiumBike() : counter(nullptr), ringer(nullptr) {}
    PremiumBike(const PremiumBike &);
    virtual ~PremiumBike();

    PremiumBike & operator=(const PremiumBike &);

    virtual bool ride(float);
    virtual void setup();
    virtual void write(std::ostream &) const;
    virtual void read(std::istream &);

    std::string ring() const;
    bool ring(std::ostream &) const;
    float get_count() const;

    bool has_counter() const { return (look_counter() != nullptr); }
    bool has_ringer() const { return (look_ringer() != nullptr); }
    bool has_rack() const { return (look_rack() != nullptr); }
    bool has_spare() const { return (look_spare() != nullptr); }

    const Rack * look_rack() const { return frame.look_rack(); }
    const Wheel * look_spare() const { return frame.look_spare(); }
    const Counter * look_counter() const { return counter; }
    const Ringer * look_ringer() const { return ringer; }

    bool add_counter(Counter *);
    bool add_ringer(Ringer *);
    bool add_rack(Rack *rck) { return frame.add_rack(rck); }
    bool add_spare(Wheel *);

    void delete_counter();
    void delete_ringer();
    void delete_rack();
    bool delete_spare();

    bool new_counter();
    bool new_ringer();
    bool new_ringer(const std::string &);
    bool new_rack();
    bool new_spare(float);

    bool swap_front_spare();
    bool swap_back_spare();
    
protected:
    Counter *counter;
    Ringer *ringer;
};