#pragma once
#include <string>
#include <iostream>

class Vehicle {
public:
    virtual ~Vehicle() = default;

    virtual bool ride(float) = 0;
    virtual void setup() = 0;
    virtual void write(std::ostream &) const = 0;
    virtual void read(std::istream &) = 0;
};

std::ostream & operator<<(std::ostream &, const Vehicle &);
std::istream & operator>>(std::istream &, Vehicle &);