#include "ringer.hpp"

const std::string & Ringer::ring() const {
    return ring_sound;
}

bool Ringer::operator==(const Ringer &ringer) const {
    return (ring() == ringer.ring());
}

bool Ringer::operator==(const std::string &string) const {
    return (ring() == string);
}

void Ringer::write(std::ostream &out) const {
    if (ring_sound == "") {
        out << "~ ";
    } else {
        out << ring_sound << " ";
    }
}

void Ringer::read(std::istream &in) {
    std::string tmp;
    in >> tmp;
    if (tmp == "~") {
        ring_sound = "";
    } else {
        ring_sound = tmp;
    }
}

bool operator==(const std::string &string, const Ringer &ringer) {
    return (string == ringer.ring());
}

std::ostream & operator<<(std::ostream &out, const Ringer &ringer) {
    ringer.write(out);
    return out;
}

std::istream & operator>>(std::istream &in, Ringer &ringer) {
    ringer.read(in);
    return in;
}