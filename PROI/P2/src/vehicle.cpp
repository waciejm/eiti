#include "vehicle.hpp"

std::ostream & operator<<(std::ostream &out, const Vehicle &vh) {
    vh.write(out);
    return out;
}

std::istream & operator>>(std::istream &in, Vehicle &vh) {
    vh.read(in);
    return in;
}