#include "counter.hpp"

void Counter::increase(float inc) {
    if (inc < 0) inc = -inc;
    count += inc;
}

float Counter::get_count() const {
    return count;
}

bool Counter::operator==(const Counter &counter) const {
    return (this->count == counter.get_count());
}

void Counter::write(std::ostream &out) const {
    out << count << " ";
}

void Counter::read(std::istream &in) {
    in >> count;
}

bool Counter::operator>(const Counter &counter) const {
    return (this->count > counter.get_count());
}

bool Counter::operator<(const Counter &counter) const {
    return (this->count < counter.get_count());
}

std::ostream & operator<<(std::ostream &out, const Counter &counter) {
    counter.write(out);
    return out;
}

std::istream & operator>>(std::istream &in, Counter &counter) {
    counter.read(in);
    return in;
}