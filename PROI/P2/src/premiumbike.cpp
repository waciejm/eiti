#include <functional>
#include "question.hpp"
#include "premiumbike.hpp"

PremiumBike::PremiumBike(const PremiumBike &bike) :
    Bike::Bike(bike),
    counter(bike.has_counter() ? new Counter(*(bike.counter)) : nullptr),
    ringer(bike.has_ringer() ? new Ringer(*(bike.ringer)) : nullptr) {}

PremiumBike::~PremiumBike() {
    if (has_counter()) delete counter;
    if (has_ringer()) delete ringer;
}

PremiumBike & PremiumBike::operator=(const PremiumBike &bike) {
    if (this == &bike) return *this;
    if (has_counter()) delete_counter();
    if (has_ringer()) delete_ringer();
    if (bike.has_counter()) counter = new Counter(*(bike.look_counter()));
    if (bike.has_ringer()) ringer = new Ringer(*(bike.look_ringer()));
    frame = bike.frame;
    return *this;
}

bool PremiumBike::ride(float distance) {
    if (!has_front_wheel() and !has_back_wheel()) return false;
    else {
        if (has_front_wheel()) frame.roll_wheel(0, distance);
        if (has_back_wheel()) frame.roll_wheel(1, distance);
        if (has_counter()) counter->increase(distance);
    }
    return true;
}

void PremiumBike::setup() {
    Bike::setup();
    if (question::get_answer("Modify rack (y/n)", "y", "n")) {
        delete_rack();
        if (question::get_answer("Rack on bike? (y/n)", "y", "n")) {
            new_rack();
            if (question::get_answer("Spare in rack? (y/n)", "y", "n")) {
                new_spare(question::get_float("Wheel diameter: ",
                         [&](float f){ if (f > 0) return true; else return false; }));
            }
        }
    }
    if (question::get_answer("Modify ringer? (y/n)", "y", "n")) {
        delete_ringer();
        if (question::get_answer("Ringer on bike? (y/n)", "y", "n")) {
            new_ringer(question::get_string("Ringer sound:"));
        }
    }
    if (question::get_answer("Modify counter? (y/n)", "y", "n")) {
        delete_counter();
        if (question::get_answer("Counter on bike? (y/n)", "y", "n")) {
            new_counter();
        }
    }
}

void PremiumBike::write(std::ostream &out) const {
    out << "PREMIUMBIKE ";
    Bike::write(out);
    if (counter == nullptr) {
        out << "~COUNTER ";
    } else {
        out <<"COUNTER ";
        counter->write(out);
    }
    if (ringer == nullptr) {
        out << "~RINGER ";
    } else {
        out <<"RINGER ";
        ringer->write(out);
    }
}

void PremiumBike::read(std::istream &in) {
    std::string tmp;
    in >> tmp;
    Bike::read(in);
    if (counter != nullptr) delete counter;
    in >> tmp;
    if (tmp == "~COUNTER") {
        counter = nullptr;
    } else {
        counter = new Counter();
        counter->read(in);
    }
    if (ringer != nullptr) delete ringer;
    in >> tmp;
    if (tmp == "~RINGER") {
        ringer = nullptr;
    } else {
        ringer = new Ringer();
        ringer->read(in);
    }
}

std::string PremiumBike::ring() const {
    if (!has_ringer()) return "";
    return ringer->ring();
}

bool PremiumBike::ring(std::ostream &stream) const{
    if (!has_ringer()) return false;
    stream << ringer->ring();
    return true;
}

float PremiumBike::get_count() const {
    if (!has_counter()) return 0;
    return counter->get_count();
}

bool PremiumBike::add_counter(Counter *cnt) {
    if (has_counter()) return false;
    counter = cnt;
    return true;
}

bool PremiumBike::add_ringer(Ringer *rng) {
    if (has_ringer()) return false;
    ringer = rng;
    return true;
}

bool PremiumBike::add_spare(Wheel *whl) {
    if (frame.look_rack() == nullptr) return false;
    return frame.add_spare(whl);
}

void PremiumBike::delete_counter() {
    if (has_counter()) {
        delete counter;
        counter = nullptr;
    }
}

void PremiumBike::delete_ringer() {
    if (has_ringer()) {
        delete ringer;
        ringer = nullptr;
    }
}

void PremiumBike::delete_rack() {
    if (has_rack()) delete frame.take_rack();
}

bool PremiumBike::delete_spare() {
    if (!has_rack()) return false;
    if (has_spare()) delete frame.take_spare();
    return true;
}

bool PremiumBike::new_counter() {
    if (has_counter()) return false;
    Counter *tmp = new Counter();
    if (!add_counter(tmp)) {
        delete tmp;
        return false;
    }
    return true;
}

bool PremiumBike::new_ringer() {
    if (has_ringer()) return false;
    Ringer *tmp = new Ringer();
    if (!add_ringer(tmp)) {
        delete tmp;
        return false;
    }
    return true;
}

bool PremiumBike::new_ringer(const std::string &str) {
    if (has_ringer()) return false;
    Ringer *tmp = new Ringer(str);
    if (!add_ringer(tmp)) {
        delete tmp;
        return false;
    }
    return true;
}

bool PremiumBike::new_rack() {
    if (has_rack()) return false;
    Rack *tmp = new Rack();
    if (!add_rack(tmp)) {
        delete tmp;
        return false;
    }
    return true;
}

bool PremiumBike::new_spare(float d) {
    if (!has_rack()) return false;
    if (has_spare()) return false;
    return add_spare(new Wheel(d));
}

bool PremiumBike::swap_front_spare() {
    return frame.swap_with_spare(0);
}

bool PremiumBike::swap_back_spare() {
    return frame.swap_with_spare(1);
}