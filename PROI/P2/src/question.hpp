#pragma once
#include <functional>
#include <string>

namespace question {
    bool get_answer(std::string, std::string, std::string);
    float get_float(std::string, std::function<bool (float)>);
    std::string get_string(std::string);
    int get_int(std::string, std::function<bool (int)>);
    int triple(std::string, std::string, std::string, std::string);
}