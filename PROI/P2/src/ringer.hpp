#pragma once
#include <string>
#include <iostream>

class Ringer {
    public:
        Ringer() : ring_sound("Ring!") {}
        Ringer(const Ringer &) = default;
        Ringer(const std::string &str) : ring_sound(str) {}

        Ringer & operator=(const Ringer &) = default;
        void write(std::ostream &) const;
        void read(std::istream &);
        
        const std::string & ring() const;
        bool operator==(const Ringer &) const;
        bool operator==(const std::string &) const;
        operator const std::string&() const { return ring_sound; }

    protected:
        std::string ring_sound;
};

bool operator==(const std::string &, const Ringer &);

std::ostream & operator<<(std::ostream &, const Ringer &);
std::istream & operator>>(std::istream &, Ringer &);