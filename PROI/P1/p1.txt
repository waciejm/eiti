> Aplikacja ma być oparta na zestawie klas, z których głównym obiektem
> będzie element, którym zarządzamy (podany indywidualnie). Obiekt ten
> ma być złożony z minimum 5 podobiektów, w tym co najmniej jednego
> tworzonego dynamicznie i jednego tworzonego automatycznie. Proszę
> wymyślić te podobiekty, ich cechy, zależności itp. i jak
> najprecyzyjniej je zdefiniować. Obiekt główny powinien umożliwiać
> tworzenie i usuwanie podobiektów dynamicznych, a wszystkie metody z
> nich korzystające muszą sprawdzać ich istnienie (ewentualnie
> modyfikując swoje działanie). Odwzorowanie powinno być możliwie
> realistyczne – dla skomplikowanych obiektów odpowiednio uproszczone.
> Każda klasa powinna prawidłowo zachowywać się w przypadku kopiowania.
> Należy zaprojektować i zaimplementować dla klas kilka sensownych,
> różnorodnych operatorów (minimum 5), w tym: jedno-, dwu-,
> trójargumentowe, konwersji, przypisania, porównania, indeksowe itp.
> (zaproponować zestaw operatorów).
> Napisać program testujący klasę główną i jej podklasy (oddzielny
> moduł/plik). Dla testów należy stworzyć obiekty automatyczne,
> dynamiczne i statyczne (lokalne, globalne) w funkcji testowej
> wywoływanej z funkcji main. Taki zestaw DEMO.
> W osobnej funkcji, wywoływanej w funkcji main jedynie przy ustawionej
> zmiennej kompilacji _DEBUG, należy przetestować wszystkie
> zaimplementowane operatory.
> Na każdą klasę powinny przypadać 2 pliki – plik nagłówkowy .h i plik
> definicji .cpp.
