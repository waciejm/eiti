#pragma once
#include "counter.hpp"
#include "ringer.hpp"
#include "frame.hpp"
#include <ostream>

class Bike {
    public:
        Bike() : counter(nullptr), ringer(nullptr), frame() {}
        Bike(const Bike &);
        ~Bike();

        Bike & operator=(const Bike &);

        bool ride(float);
        std::string ring() const;
        bool ring(std::ostream &) const;
        float get_count() const;

        bool has_counter() const { return (look_counter() != nullptr); }
        bool has_ringer() const { return (look_ringer() != nullptr); }
        bool has_front_wheel() const { return (look_front_wheel() != nullptr); }
        bool has_back_wheel() const { return (look_back_wheel() != nullptr); }
        bool has_rack() const { return (look_rack() != nullptr); }
        bool has_spare() const { return (look_spare() != nullptr); }

        const Counter * look_counter() const { return counter; }
        const Ringer * look_ringer() const { return ringer; }
        const Wheel * look_front_wheel() const { return frame.look_wheel(0); }
        const Wheel * look_back_wheel() const { return frame.look_wheel(1); }
        const Rack * look_rack() const { return frame.look_rack(); }
        const Wheel * look_spare() const { return frame.look_spare(); }
        const Frame * look_frame() const { return &frame; }

        bool add_counter(Counter *);
        bool add_ringer(Ringer *);
        bool add_front_wheel(Wheel *whl) { return frame.add_wheel(0, whl); }
        bool add_back_wheel(Wheel *whl) { return frame.add_wheel(1, whl); }
        bool add_rack(Rack *rck) { return frame.add_rack(rck); }
        bool add_spare(Wheel *);

        void delete_counter();
        void delete_ringer();
        void delete_front_wheel();
        void delete_back_wheel();
        void delete_rack();
        bool delete_spare();

        bool new_counter();
        bool new_ringer();
        bool new_ringer(const std::string &);
        bool new_front_wheel(float);
        bool new_back_wheel(float);
        bool new_rack();
        bool new_spare(float);

        void swap_wheels();
        bool swap_front_spare();
        bool swap_back_spare();

    private:
        Counter *counter;
        Ringer *ringer;
        Frame frame;
};