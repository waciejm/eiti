#pragma once
#include <stdexcept>

class Wheel {
    public:
        Wheel(float);
        Wheel(const Wheel &) = default;

        Wheel & operator=(const Wheel &) = default;

        float get_diameter() const;
        float get_rotation() const;

        void roll(float);

    private:
        float diameter;
        float rotation = 0.0; // Radians
};