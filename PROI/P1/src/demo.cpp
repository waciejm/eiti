#include <cassert>
#include <iostream>
#include "demo.hpp"
#include "bike.hpp"

int demo() {
    std::cout << "START DEMO\n";
    std::cout << "Tworzymy nowy rower:\n";
    Bike bike1;
    print_bike_state(bike1);
    std::cout << "Probujemy pojechać, ale funkcja zwraca: " << bike1.ride(1.0) << std::endl;
    std::cout << "Zakładamy nowe przednie koło:\n";
    bike1.new_front_wheel(2.0);
    print_bike_state(bike1);
    std::cout << "Jedziemy do przodu 1.0:\n";
    bike1.ride(1.0);
    print_bike_state(bike1);
    std::cout << "Zakładamy licznik:\n";
    bike1.new_counter();
    print_bike_state(bike1);
    std::cout << "Jedziemy do przodu 2.0:\n";
    bike1.ride(2.0);
    print_bike_state(bike1);
    std::cout << "Zakładamy nowe tylnie koło:\n";
    bike1.new_back_wheel(3.0);
    print_bike_state(bike1);
    std::cout << "Jedziemy do przodu 1.0:\n";
    bike1.ride(1.0);
    print_bike_state(bike1);
    std::cout << "Zakładamy bagaznik:\n";
    bike1.new_rack();
    print_bike_state(bike1);
    std::cout << "Wkładamy do bagaznika nowe kolo:\n";
    bike1.new_spare(4.0);
    print_bike_state(bike1);
    std::cout << "Zakładamy dzwonek o dzwieku \"Reeee\"\n";
    bike1.new_ringer("Reeee");
    print_bike_state(bike1);
    std::cout << "Dzwonimy na cout:\n" << bike1.ring() << std::endl;
    std::cout << "Zamieniamy przednie koło z zapasowym:\n";
    bike1.swap_front_spare();
    print_bike_state(bike1);
    std::cout << "Jedniemy do tyłu 2.0:\n";
    bike1.ride(-2.0);
    print_bike_state(bike1);
    std::cout << "Tworzymy nowy rower:\n";
    Bike *bike2 = new Bike();
    print_bike_state(bike1);
    print_bike_state(*bike2);
    std::cout << "Kopiujemy stan pierwszego do drugiego:\n";
    *bike2 = bike1;
    print_bike_state(bike1);
    print_bike_state(*bike2);
    std::cout << "Pierwszy jedzie do przodu 1.0:\n";
    bike1.ride(1.0);
    print_bike_state(bike1);
    print_bike_state(*bike2);
    std::cout << "Usuwamy drugiemy rowerowi bagaznik i licznik: \n";
    bike2->delete_rack();
    bike2->delete_counter();
    print_bike_state(bike1);
    print_bike_state(*bike2);
    std::cout << "Usuwamy drugi rower:\n";
    delete bike2;
    print_bike_state(bike1);

    std::cout << "END DEMO\n";
    return 0;
}

void print_bike_state(const Bike &bike) {
    std::cout << "-----BIKE STATE-----\n";
    std::cout << "Ringer: " << (bike.has_ringer() ? bike.ring() : "none") << std::endl;
    std::cout << "Counter: ";
    if (bike.has_counter()) {
        std::cout << bike.get_count() << std::endl;
    } else {
        std::cout << "none\n";
    }
    std::cout << "Front wheel: ";
    if (bike.has_front_wheel()) {
        std::cout << "Diameter: " << bike.look_front_wheel()->get_diameter() << "   Rotation: "
                  << bike.look_front_wheel()->get_rotation() << std::endl;
    } else {
        std::cout << "none\n";
    }
    std::cout << "Back wheel : ";
    if (bike.has_back_wheel()) {
        std::cout << "Diameter: " << bike.look_back_wheel()->get_diameter() << "   Rotation: "
                  << bike.look_back_wheel()->get_rotation() << std::endl;
    } else {
        std::cout << "none\n";
    }
    if (bike.has_rack()) {
        std::cout << "Has rack!\nSpare: ";
        if (bike.has_spare()) {
            std::cout << "Diameter: " << bike.look_spare()->get_diameter() << "   Rotation: "
                      << bike.look_spare()->get_rotation() << std::endl;
        } else {
            std::cout << "none\n";
        }
    } else {
        std::cout << "No rack!\n";
    }
    std::cout << "---------------------\n";
}

int test_it_all() {
    std::cout << "TESTING...\n";
    // Counter
    {
        Counter counter;
        assert(counter.get_count() == 0.0);
        counter.increase(1.0);
        assert(counter.get_count() == 1.0);
        counter += 1.0;
        assert(counter.get_count() == 2.0);
        assert(counter == 2.0);
        std::cout << "Below you should see 2\n" << counter << std::endl;
        Counter counter2;
        assert(counter > counter2);
        assert(!(counter < counter2));
        assert(!(counter == counter2));
        counter2 += 2.0;
        assert(counter == counter2);
        counter2 += 1.0;
        assert(counter < counter2);
        Counter counter3(counter);
        assert(counter == counter3);
        assert(&counter != &counter3);
        Counter counter4;
        counter4 = counter2;
        assert(counter4 == counter2);
    }

    // Ringer
    {
        Ringer ringer;
        assert(ringer.ring() == "Ring!");
        std::cout << "Below you should see Ring!\n" << ringer << std::endl;
        Ringer ringer2("Reeee");
        assert(ringer2.ring() == "Reeee");
        std::cout << "Below you should see Reeee\n" << ringer2 << std::endl;
        std::string string = ringer2;
        assert(string == ringer2);
        assert(ringer2 == string);
        Ringer ringer3(ringer);
        assert(ringer3 == ringer);
        assert(&ringer != &ringer3);
        Ringer ringer4;
        ringer4 = ringer2;
        assert(ringer4 == ringer2);
    }

    // Wheel
    {
        Wheel wheel(1.0);
        assert(wheel.get_rotation() == 0.0);
        assert(wheel.get_diameter() == 1.0);
        wheel.roll(1.0);
        assert(wheel.get_rotation() == 2.0);
        wheel.roll(-0.5);
        assert(wheel.get_rotation() == 1.0);
        wheel.roll(-0.51);
        assert(wheel.get_rotation() > 6.0);
        std::cout << "You should see an invalid argument exception below\n";
        try {
            Wheel(-1);
        } catch (const std::invalid_argument &ia) {
            std::cout << "Invalid argument: " << ia.what() << std::endl;
        }
        Wheel wheel2(wheel);
        assert(wheel2.get_diameter() == wheel.get_diameter());
        assert(wheel2.get_rotation() == wheel.get_rotation());
        assert(&wheel2 != &wheel);
        Wheel wheel3(10.0);
        assert(wheel3.get_diameter() != wheel2.get_diameter());
        wheel3 = wheel2;
        assert(wheel3.get_diameter() == wheel2.get_diameter());

    }

    // Rack
    {
        Rack rack = Rack();
        assert(rack.look_spare() == nullptr);
        Wheel *wheel = new Wheel(2.0);
        assert(rack.add_spare(wheel));
        assert(!rack.add_spare(wheel));
        assert(rack.look_spare() == wheel);
        assert(rack.take_spare() == wheel);
        assert(rack.take_spare() == nullptr);
        assert(rack.look_spare() == nullptr);
        assert(rack.add_spare(wheel));
        Rack rack2(rack);
        assert(rack.look_spare() != rack2.look_spare());
        assert(rack.look_spare()->get_diameter() == rack2.look_spare()->get_diameter());
        Rack rack3;
        rack3 = rack2;
        assert(rack3.look_spare() != rack2.look_spare());
        assert(rack3.look_spare()->get_diameter() == rack2.look_spare()->get_diameter());
    }

    // Frame
    {
        Wheel *wheel0 = new Wheel(3.0);
        Wheel *wheel1 = new Wheel(4.0);
        Wheel *wheel2 = new Wheel(5.0);
        Rack *rack = new Rack();
        std::cout << "You should see an invalid argument exception below\n";
        try {
            Frame frame = Frame(wheel0, wheel0);
        } catch (const std::invalid_argument &ia) {
            std::cout << "Invalid argument: " << ia.what() << std::endl;
        }
        Frame frame = Frame(wheel0, wheel1, rack);
        assert(frame.look_wheel(0) == wheel0);
        assert(frame[0] == wheel0);
        assert(frame.look_wheel(1) == wheel1);
        assert(frame[1] == wheel1);
        assert(frame.look_rack() == rack);
        assert(frame.take_wheel(0) == wheel0);
        assert(frame[0] == nullptr);
        assert(frame.take_wheel(0) == nullptr);
        assert(frame.take_wheel(1) == wheel1);
        assert(frame[1] == nullptr);
        assert(frame.take_wheel(1) == nullptr);
        assert(frame.take_rack() == rack);
        assert(frame.look_rack() == nullptr);
        assert(frame.take_rack() == nullptr);
        assert(!frame.swap_with_spare(0));
        assert(frame.add_rack(rack));
        assert(frame.add_spare(wheel2));
        assert(frame.look_spare() == wheel2);
        assert(frame.take_spare() == wheel2);
        assert(frame.look_spare() == nullptr);
        assert(!frame.roll_wheel(0, 1.0));
        assert(!frame.roll_wheel(1, 1.0));
        assert(frame.add_wheel(0, wheel0));
        assert(frame.add_wheel(1, wheel1));
        assert(frame.add_spare(wheel2));
        assert(frame.roll_wheel(0, 1.0));
        assert(frame[0]->get_rotation() > 0.0);
        assert(frame[1]->get_rotation() == 0.0);
        frame.swap_wheels();
        assert(frame[0] == wheel1);
        assert(frame[1] == wheel0);
        assert(frame.swap_with_spare(0));
        assert(frame[0] == wheel2);
        assert(frame.look_spare() == wheel1);
        Frame frame2(frame);
        Frame frame3;
        frame3 = frame2;
        assert(frame3.look_wheel(0) != frame2.look_wheel(0));
        assert(frame3.look_spare() != frame2.look_spare());
        assert(frame3.look_wheel(1)->get_diameter() == frame2.look_wheel(1)->get_diameter());
    }

    // Bike
    {
        Bike bike = Bike();
        assert(!bike.ride(1.0));
        assert(!bike.has_counter());
        assert(!bike.has_ringer());
        assert(!bike.has_front_wheel());
        assert(!bike.has_back_wheel());
        assert(!bike.has_rack());
        assert(!bike.has_spare());
        Ringer *ringer = new Ringer("ringer");
        Counter *counter = new Counter();
        Wheel *wheel0 = new Wheel(1.0);
        Wheel *wheel1 = new Wheel(2.0);
        Wheel *wheel2 = new Wheel(3.0);
        Rack *rack = new Rack();
        assert(bike.add_counter(counter));
        assert(bike.ring() == "");
        assert(!bike.ring(std::cout));
        assert(bike.add_ringer(ringer));
        assert(bike.ring() == "ringer");
        assert(bike.add_front_wheel(wheel0));
        assert(bike.add_back_wheel(wheel1));
        assert(!bike.add_spare(wheel2));
        assert(bike.add_rack(rack));
        assert(bike.add_spare(wheel2));
        assert(bike.look_counter() == counter);
        assert(bike.look_ringer() == ringer);
        assert(bike.look_front_wheel() == wheel0);
        assert(bike.look_back_wheel() == wheel1);
        assert(bike.look_rack() == rack);
        assert(bike.look_spare() == wheel2);
        bike.delete_counter();
        assert(!bike.has_counter());
        bike.delete_ringer();
        assert(!bike.has_ringer());
        bike.delete_front_wheel();
        assert(!bike.has_front_wheel());
        bike.delete_back_wheel();
        assert(!bike.has_back_wheel());
        assert(bike.delete_spare());
        assert(!bike.has_spare());
        bike.delete_rack();
        assert(!bike.has_rack());
        assert(bike.new_counter());
        assert(!bike.new_counter());
        assert(bike.has_counter());
        assert(bike.new_ringer("ring"));
        assert(!bike.new_ringer());
        assert(bike.has_ringer());
        assert(bike.ring() == "ring");
        std::cout << "Below you should see ring\n";
        assert(bike.ring(std::cout));
        std::cout << std::endl;
        assert(bike.new_front_wheel(4.0));
        assert(!bike.new_front_wheel(1.0));
        assert(bike.has_front_wheel());
        assert(bike.new_back_wheel(5.0));
        assert(!bike.new_back_wheel(1.0));
        assert(bike.has_front_wheel());
        assert(!bike.new_spare(1.0));
        assert(bike.new_rack());
        assert(!bike.new_rack());
        assert(bike.has_rack());
        assert(!bike.has_spare());
        assert(bike.new_spare(6.0));
        assert(!bike.new_spare(1.0));
        assert(bike.has_spare());
        assert(bike.look_front_wheel()->get_diameter() == 4.0);
        assert(bike.look_back_wheel()->get_diameter() == 5.0);
        bike.swap_wheels();
        assert(bike.look_back_wheel()->get_diameter() == 4.0);
        assert(bike.look_front_wheel()->get_diameter() == 5.0);
        bike.swap_wheels();
        assert(bike.swap_front_spare());
        assert(bike.look_front_wheel()->get_diameter() == 6.0);
        assert(bike.look_spare()->get_diameter() == 4.0);
        assert(bike.swap_front_spare());
        assert(bike.swap_back_spare());
        assert(bike.look_back_wheel()->get_diameter() == 6.0);
        assert(bike.look_spare()->get_diameter() == 5.0);
        assert(bike.swap_back_spare());
        assert(bike.get_count() == 0.0);
        assert(bike.ride(1.0));
        assert(bike.get_count() == 1.0);
        assert(bike.look_front_wheel()->get_rotation() > 0.0);
        assert(bike.look_back_wheel()->get_rotation() > 0.0);
        assert(bike.look_spare()->get_rotation() == 0.0);
        Bike bike2(bike);
        Bike bike3;
        bike3 = bike2;
        assert(bike3.look_front_wheel() != bike2.look_front_wheel());
    }

    std::cout << "END TESTING\n";
    return 0;
}