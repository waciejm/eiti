#include "wheel.hpp"

#include <cmath>

Wheel::Wheel(float d) {
    if (d <= 0) throw std::invalid_argument("Wheel::Wheel(float): Non-positive float passed!");
    diameter = d;
}

float Wheel::get_diameter() const {
    return diameter;
}

float Wheel::get_rotation() const {
    return rotation;
}

void Wheel::roll(float distance) {
    rotation = fmod(rotation + (distance / (diameter / 2.0)), 2.0 * M_PI);
    if (rotation < 0) rotation = 2 * M_PI + rotation;
}