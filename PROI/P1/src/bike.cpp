#include "bike.hpp"

Bike::Bike(const Bike &bike) :
    counter(bike.has_counter() ? new Counter(*(bike.counter)) : nullptr),
    ringer(bike.has_ringer() ? new Ringer(*(bike.ringer)) : nullptr),
    frame(bike.frame) {}

Bike::~Bike() {
    if (has_counter()) delete counter;
    if (has_ringer()) delete ringer;
}

Bike & Bike::operator=(const Bike &bike) {
    if (this == &bike) return *this;
    if (has_counter()) delete_counter();
    if (has_ringer()) delete_ringer();
    if (bike.has_counter()) counter = new Counter(*(bike.look_counter()));
    if (bike.has_ringer()) ringer = new Ringer(*(bike.look_ringer()));
    frame = bike.frame;
    return *this;
}

bool Bike::ride(float distance) {
    if (!has_front_wheel() and !has_back_wheel()) return false;
    else {
        if (has_front_wheel()) frame.roll_wheel(0, distance);
        if (has_back_wheel()) frame.roll_wheel(1, distance);
        if (has_counter()) counter->increase(distance);
    }
    return true;
}

std::string Bike::ring() const {
    if (!has_ringer()) return "";
    return ringer->ring();
}

bool Bike::ring(std::ostream &stream) const{
    if (!has_ringer()) return false;
    stream << ringer->ring();
    return true;
}

float Bike::get_count() const {
    if (!has_counter()) return 0;
    return counter->get_count();
}

bool Bike::add_counter(Counter *cnt) {
    if (has_counter()) return false;
    counter = cnt;
    return true;
}

bool Bike::add_ringer(Ringer *rng) {
    if (has_ringer()) return false;
    ringer = rng;
    return true;
}

bool Bike::add_spare(Wheel *whl) {
    if (frame.look_rack() == nullptr) return false;
    return frame.add_spare(whl);
}

void Bike::delete_counter() {
    if (has_counter()) {
        delete counter;
        counter = nullptr;
    }
}

void Bike::delete_ringer() {
    if (has_ringer()) {
        delete ringer;
        ringer = nullptr;
    }
}

void Bike::delete_front_wheel() {
    if (has_front_wheel()) delete frame.take_wheel(0);
}

void Bike::delete_back_wheel() {
    if (has_back_wheel()) delete frame.take_wheel(1);
}

void Bike::delete_rack() {
    if (has_rack()) delete frame.take_rack();
}

bool Bike::delete_spare() {
    if (!has_rack()) return false;
    if (has_spare()) delete frame.take_spare();
    return true;
}

bool Bike::new_counter() {
    if (has_counter()) return false;
    Counter *tmp = new Counter();
    if (!add_counter(tmp)) {
        delete tmp;
        return false;
    }
    return true;
}

bool Bike::new_ringer() {
    if (has_ringer()) return false;
    Ringer *tmp = new Ringer();
    if (!add_ringer(tmp)) {
        delete tmp;
        return false;
    }
    return true;
}

bool Bike::new_ringer(const std::string &str) {
    if (has_ringer()) return false;
    Ringer *tmp = new Ringer(str);
    if (!add_ringer(tmp)) {
        delete tmp;
        return false;
    }
    return true;
}

bool Bike::new_front_wheel(float d) {
    if (has_front_wheel()) return false;
    Wheel *tmp = new Wheel(d);
    if (!add_front_wheel(tmp)) {
        delete tmp;
        return false;
    }
    return true;
}

bool Bike::new_back_wheel(float d) {
    if (has_back_wheel()) return false;
    Wheel *tmp = new Wheel(d);
    if (!add_back_wheel(tmp)) {
        delete tmp;
        return false;
    }
    return true;
}

bool Bike::new_rack() {
    if (has_rack()) return false;
    Rack *tmp = new Rack();
    if (!add_rack(tmp)) {
        delete tmp;
        return false;
    }
    return true;
}

bool Bike::new_spare(float d) {
    if (!has_rack()) return false;
    if (has_spare()) return false;
    return add_spare(new Wheel(d));
}

void Bike::swap_wheels() {
    return frame.swap_wheels();
}

bool Bike::swap_front_spare() {
    return frame.swap_with_spare(0);
}

bool Bike::swap_back_spare() {
    return frame.swap_with_spare(1);
}