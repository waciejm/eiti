#pragma once
#include <stdexcept>
#include "wheel.hpp"
#include "rack.hpp"

class Frame {
    public:
        Frame(Wheel * = nullptr, Wheel * = nullptr, Rack * = nullptr);
        Frame(const Frame &);
        ~Frame();

        Frame & operator=(const Frame &);

        const Wheel * look_wheel(int) const;
        Wheel * take_wheel(int);
        bool add_wheel(int, Wheel *);
        const Wheel * operator[](int id) const { return look_wheel(id); }
        bool roll_wheel(int, float);

        const Rack * look_rack() const;
        Rack * take_rack();
        bool add_rack(Rack *);

        const Wheel * look_spare() const;
        Wheel * take_spare();
        bool add_spare(Wheel *);

        void swap_wheels();
        bool swap_with_spare(int);

    private:
        Wheel *wheel[2];
        Rack *rack;
};