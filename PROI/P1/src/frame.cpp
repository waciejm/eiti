#include "frame.hpp"

Frame::Frame(Wheel *front, Wheel *back, Rack *rck) {
    if (front != nullptr && front == back) {
        throw std::invalid_argument("Frame::Frame(Wheel *, Wheel *, Rack *): The same wheel ptr passed twice!");
    }
    wheel[0] = front;
    wheel[1] = back;
    rack = rck;
}

Frame::Frame(const Frame &frame){
    for (int i = 0; i < 2; ++i) {
        if (frame.wheel[i] != nullptr) wheel[i] = new Wheel(*(frame.wheel[i]));
        else wheel[i] = nullptr;
    }
    if (frame.rack != nullptr) rack = new Rack(*(frame.rack));
    else rack = nullptr;
}

Frame::~Frame() {
    for (int i = 0; i < 2; ++i) {
        if (wheel[i] != nullptr) delete wheel[i];
    }
    if (rack != nullptr) delete rack;
}

Frame & Frame::operator=(const Frame &frame) {
    if (this == &frame) return *this;
    for (int i = 0; i < 2; ++i) {
        if (wheel[i] != nullptr) {
            delete wheel[i];
            wheel[i] = nullptr;
        }
        if (frame.look_wheel(i) != nullptr) wheel[i] = new Wheel(*(frame.look_wheel(i)));
    }
    if (rack != nullptr) {
        delete rack;
        rack = nullptr;
    }
    if (frame.rack != nullptr) rack = new Rack(*(frame.look_rack()));
    return *this;
}

bool in_frame(int id) {
    if (id < 0 || id > 1) return false;
    return true;
}

const Wheel * Frame::look_wheel(int id) const {
    if (!in_frame(id)) return nullptr;
    return wheel[id];
}

Wheel * Frame::take_wheel(int id) {
    if (!in_frame(id)) return nullptr;
    Wheel * tmp = wheel[id];
    wheel[id] = nullptr;
    return tmp;
}

bool Frame::add_wheel(int id, Wheel *whl) {
    if (!in_frame(id) || wheel[id] != nullptr) return false;
    if (whl != nullptr && whl == wheel[1-id]) return false;
    wheel[id] = whl;
    return true;
}

bool Frame::roll_wheel(int id, float distance) {
    if (!in_frame(id) || wheel[id] == nullptr) return false;
    wheel[id]->roll(distance);
    return true;
}

const Rack * Frame::look_rack() const {
    return rack;
}

Rack * Frame::take_rack() {
    Rack * tmp = rack;
    rack = nullptr;
    return tmp;
}

bool Frame::add_rack(Rack *rck) {
    if (rack != nullptr) return false;
    rack = rck;
    return true;
}

const Wheel * Frame::look_spare() const {
    if (rack == nullptr) return nullptr;
    return rack->look_spare();
}

Wheel * Frame::take_spare() {
    if (rack == nullptr) return nullptr;
    return rack->take_spare();
}

bool Frame::add_spare(Wheel *whl) {
    if (rack == nullptr) return false;
    return rack->add_spare(whl);
}

void Frame::swap_wheels() {
    Wheel *tmp = wheel[0];
    wheel[0] = wheel[1];
    wheel[1] = tmp;
}

bool Frame::swap_with_spare(int id) {
    if (!in_frame(id)) return false;
    if (rack == nullptr) return false;
    Wheel * tmp = rack->take_spare();
    rack->add_spare(take_wheel(id));
    add_wheel(id, tmp);
    return true;
}