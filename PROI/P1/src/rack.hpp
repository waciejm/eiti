#pragma once
#include "wheel.hpp"

class Rack {
    public:
        Rack() : spare(nullptr) {}
        Rack(const Rack &);
        ~Rack();

        Rack & operator=(const Rack &);

        bool add_spare(Wheel *);
        Wheel * take_spare();
        const Wheel * look_spare() const;

        operator const Wheel*() const { return spare; }
        bool operator+=(Wheel *wheel) { return add_spare(wheel); }

    private:
        Wheel *spare;
};