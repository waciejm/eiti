#include "ringer.hpp"

const std::string & Ringer::ring() const {
    return ring_sound;
}

bool Ringer::operator==(const Ringer &ringer) const {
    return (ring() == ringer.ring());
}

bool Ringer::operator==(const std::string &string) const {
    return (ring() == string);
}

bool operator==(const std::string &string, const Ringer &ringer) {
    return (string == ringer.ring());
}

std::ostream & operator<<(std::ostream &stream, const Ringer &ringer) {
    return stream << ringer.ring();
}