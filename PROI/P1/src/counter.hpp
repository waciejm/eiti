#pragma once
#include <ostream>

class Counter {
    public:
        Counter() : count(0.0) {}
        Counter(const Counter &) = default;

        Counter & operator=(const Counter &) = default;

        void increase(float);
        void operator+=(float inc) { return increase(inc); }
        float get_count() const;

        operator float() const { return count; }
        bool operator==(const Counter &) const;
        bool operator>(const Counter &) const;
        bool operator<(const Counter &) const;

    private:
        float count = 0.0;
};

std::ostream & operator<<(std::ostream &, const Counter &);