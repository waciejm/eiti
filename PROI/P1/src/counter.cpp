#include "counter.hpp"

void Counter::increase(float inc) {
    if (inc < 0) inc = -inc;
    count += inc;
}

float Counter::get_count() const {
    return count;
}

bool Counter::operator==(const Counter &counter) const {
    return (this->count == counter.get_count());
}

bool Counter::operator>(const Counter &counter) const {
    return (this->count > counter.get_count());
}

bool Counter::operator<(const Counter &counter) const {
    return (this->count < counter.get_count());
}

std::ostream & operator<<(std::ostream &stream, const Counter &counter) {
    return stream << counter.get_count();
}