#include <iostream>
#include "rack.hpp"

Rack::Rack(const Rack & rack) {
    if (rack.spare != nullptr) spare = new Wheel(*(rack.spare));
    else spare = nullptr;
}

Rack::~Rack() {
    if (spare != nullptr) delete spare;
}

Rack & Rack::operator=(const Rack &rack) {
    if (this == &rack) return *this;
    if (spare != nullptr) {
        delete spare;
        spare = nullptr;
    }
    if (rack.spare != nullptr) {
        spare = new Wheel(*(rack.spare));
    }
    return *this;
}

Wheel * Rack::take_spare() {
    if (spare == nullptr) return nullptr;
    Wheel *tmp = spare;
    spare = nullptr;
    return tmp;
}

bool Rack::add_spare(Wheel * wheel) {
    if (spare != nullptr) return false;
    spare = wheel;
    return true;
}

const Wheel * Rack::look_spare() const {
    return spare;
}